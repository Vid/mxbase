// jest.config.js
module.exports = {
  // [...]
  // Replace `ts-jest` with the preset you want to use
  // from the above list
  testMatch: ['<rootDir>/**/?(*.)test.{ts,tsx}'],
  "setupFilesAfterEnv": ["<rootDir>/src/setupBail.ts"],
  preset: 'ts-jest',
  "globals": {
    "ts-jest": {
      diagnostics: true,
    }
  }
};
