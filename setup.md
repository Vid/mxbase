

To use this library with another project (until it's published);

`npm link`

To work properly, you will need to set up a front-end server, see the NGINX section.
Get a wildcard certificate for your domain (see below).

Then create your instance in a new directory, with at least a site/index.js that has something like

```
module.exports = {
  pages: {},
  base: '/public/(overview)/',
  index: 'TOC',
  display: {},
  host: 'https://solid.your.domain'
}
```

In the new directory, do this:

* `npm link mxbase`
* `node node_modules/mxbase/bin/generate-key.js`
* `npm node_modules/mxbase/bin/test-sign.js` (optional, do a test sign)


Then start it with something like `DEBUG="solid:*" nodemon node_modules/.bin/base`

## NGINX setup


Create a site config like this:

```
map $http_upgrade $connection_upgrade {
    default upgrade;
      ''      close;
}

server {
  listen 80;
  server_name     your.domain *.your.domain;
  return 301 https://$host$request_uri;
}

server {
  server_name     your.domain *.your.domain;
  error_page 404 /;

  listen          443;
  ssl             on;

  default_type  application/octet-stream;
  sendfile        on;
  keepalive_timeout  65;

  gzip on;
  gzip_comp_level 6;
  gzip_vary on;
  gzip_min_length  1000;
  gzip_proxied any;
  gzip_buffers 16 8k;

  ssl_protocols        TLSv1 TLSv1.1 TLSv1.2;
  ssl_ciphers RC4:HIGH:!aNULL:!MD5;
  ssl_prefer_server_ciphers on;
  ssl_session_cache    shared:SSL:10m;
  ssl_session_timeout  10m;

  ssl_certificate      path.to.your.cert;
  ssl_certificate_key  path.to.your.key;

  location  /app/ {
          proxy_pass http://your.backend.host:2020;
          include "conf.d/proxy";
  }
  location  / {
          proxy_pass http://your.backend.host:2020;
          include "conf.d/proxy";
  }
  location  /a/ {
          proxy_pass http://your.backend.host:2019;
          include "conf.d/proxy";
  }
  location  /static/ {
          proxy_pass http://your.backend.host:2019;
          include "conf.d/proxy";
  }
  location  /sockjs-node/ {
          proxy_pass http://your.backend.host:2019;
          include "conf.d/proxy";
  }
}

```

For production builds (post `npm run build`), with build/ in your site instance directory, use the following for the last locations:

```

  location  /a/ {
          proxy_pass http://your.backend.host:2020;
          include "conf.d/proxy";
  }
  location  /app/ {
          proxy_pass http://your.backend.host:2020;
          include "conf.d/proxy";
  }

```

conf.d/proxy looks like this:

```
proxy_buffering off;
proxy_redirect     off;
proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_set_header Connection keep-alive;
```


## Wildcard certificate

If using letsencrypt, and CloudFlare with a challenge alias, use this script:

```
export CF_Key="your.key" 
export CF_Email="your.email.recognized.by.cloudflare"

docker run --rm  -it  \
  -v "$(pwd)/out":/acme.sh  \
  -e CF_Key \
  -e CF_Email \
  neilpang/acme.sh acme.sh --issue \
        --force \
        -d your.domain  --challenge-alias challenge-alias.host \
        -d *.your.domain --challenge-alias challenge-alias.host \
        -d *.solid.your.domain --challenge-alias challenge-alias.host \
        --dns dns_cf

```

Finally (whew), you'll need to set up wildcard DNS. For bind9, 
with a CloudFlare challenge alias using the above:

```
;Zone file for your.domain
$TTL 3600

@       IN      SOA     your.domain.     ns.your.domain. (
                                2019080108 ; Serial number
                                600     ; Refresh every X seconds
                                300     ; Retry every hour
                                604800  ; Expire every 20 days
                                21600 ) ; Minimum 2 days

                IN      NS      your.ns.server.
                IN      NS      your.ns.server.

                IN      A       your.ip.address

solid           IN      A       your.ip.address
*.solid         IN      A       your.ip.address
*               IN      A       your.ip.address


_acme-challenge IN      CNAME   _acme-challenge.challenge-alias.host.
_acme-challenge.solid IN      CNAME   _acme-challenge.challenge-alias.host.

```