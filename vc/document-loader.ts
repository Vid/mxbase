/*
 * adapted from code by Digital Bazaar
*/

import credentialsContext from 'credentials-context';
import fetch from 'isomorphic-fetch';

import { vcExamplesV1 } from './contexts/vc-examples-v1';
import { odrl } from './contexts/odrl';
import { schema } from './contexts/schema';
import { schemaInNS } from '../lib/paths';

export const getSchemas = (config) => {
  const { schemas } = config;
  const found = Object.keys(schemas).map(s => ([schemaInNS(config.host, s), schemas[s].schema]));
  return found;
}

const getContext = (config) => {
  const schemas = getSchemas(config);
  return new Map([
    ...credentialsContext.contexts,
    ...schemas,
    [
      'https://www.w3.org/2018/credentials/examples/v1',
      vcExamplesV1
    ],
    [
      'https://www.w3.org/ns/odrl.jsonld',
      odrl
    ],
    [
      'http://schema.org',
      schema
    ]
  ]);
}

export const getDocumentLoader = (config) => {
  return async (url) => {
    let document;
    const contexts = getContext(config);
    document = contexts.get(url);
    if (document) {
      return {
        contextUrl: null,
        documentUrl: url,
        document
      };
    }
    const response = await fetch(url);
    if (!response.ok) {
      throw Error(`cannot retrieve context ${url}`);
    }
    document = await response.json();
    return {
      contextUrl: null,
      documentUrl: url,
      document,
    };
  };

}