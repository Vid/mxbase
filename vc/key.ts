/*
 * Based on a sample by Digital Bazaar.
*/
import fs from 'fs';
import { Ed25519KeyPair } from 'jsonld-signatures';

import { FileWriter } from './FileWriter';
import { mkpaths } from '../lib/util';
// import Secp256k1KeyPair from 'secp256k1-key-pair';

export async function writeKeys(host, site_base, username) {
  mkpaths(site_base);
  // FIXME key owner is undefined
  const result = await generateKey(site_base, username, host + '/app/key/', 'ed25519');
  const { privateKeyBase58, controller } = result;
  fs.writeFileSync(`${site_base}/private/signing/${username}.json`, JSON.stringify({ privateKeyBase58, controller }, null, 2));
}

export const generateKey = async (site_base, username, base, keyType) => {
  let k;
  switch (keyType) {
    case 'ed25519':
      k = await Ed25519KeyPair.generate();
      break;
    // case 'secp256k1':
    //   k = await Secp256k1KeyPair.generate();
    //   break;
    default:
      throw new Error(`Unknown key type: "${keyType}".`);
  }

  const controllerWriter = new FileWriter(base, `controller.${username}.json`);
  const keyWriter = new FileWriter(base, `${username}.json`);

  const publicKeyDoc = Object.assign(k.publicNode(), {
    '@context': 'https://w3id.org/security/v2',
    id: keyWriter.rawUrl,
    controller: controllerWriter.rawUrl
  });
  const controllerDoc = {
    '@context': 'https://w3id.org/security/v2',
    id: controllerWriter.rawUrl,
    assertionMethod: [keyWriter.rawUrl]
  };

  await Promise.all([
    controllerWriter.create({ path: `${site_base}/keys`, content: controllerDoc }),
    keyWriter.create({ path: `${site_base}/keys`, content: publicKeyDoc }),
  ]);

  return Object.assign(k, {
    id: keyWriter.rawUrl,
    controller: controllerWriter.rawUrl,
  });
};
