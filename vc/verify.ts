/*
 * Based on a sample by Digital Bazaar.
*/

import vc from 'vc-js';
const { suites: { Ed25519Signature2018 } } = require('jsonld-signatures');

import { getDocumentLoader } from './';

export async function verifyCredential(credential, config) {
  const documentLoader = getDocumentLoader(config);
  const suite = new Ed25519Signature2018({
  });
  const result = await vc.verifyCredential({
    credential,
    suite,
    documentLoader,
  });
  return result;
}
