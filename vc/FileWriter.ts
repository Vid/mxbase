/*
 * Based on a sample by Digital Bazaar.
*/
import * as fs from 'fs';
import { join } from 'path';

export class FileWriter {
  filename: string;
  base: string;
  fullRawUrl: string;

  constructor(base, filename) {
    this.filename = filename;
    this.base = base;
    this.fullRawUrl = this.base + filename;
  }

  get rawUrl() {
    return this.fullRawUrl;
  }

  async create({ path, content }) {
    const out = join(path, this.filename);
    fs.writeFileSync(out, JSON.stringify(content, null, 2), null);
  }

  async update({ path, content }) {
    return this.create({ path, content })
  }
}
