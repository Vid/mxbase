/*
 * Based on a sample by Digital Bazaar.
*/

import * as fs from 'fs';
import vc from 'vc-js';
const { Ed25519KeyPair, suites: { Ed25519Signature2018 } } = require('jsonld-signatures');

import { getIso8601Date } from '../lib/data';
import { getDocumentLoader } from './';
import { schemaInNS, issuerKeyLoc } from '../lib/paths';

// retrieve the signing elements and return the signed document
export const sign = async ({ credentialID, issuer, subject, schema, config }) => {
  const publicKeyLoc = `${config.site_base}/keys/${issuer}.json`;
  const signingKeyLoc = `${config.site_base}/private/signing/${issuer}.json`;

  const publicKey = JSON.parse(fs.readFileSync(publicKeyLoc, 'utf-8'));
  const signingKey = JSON.parse(fs.readFileSync(signingKeyLoc, 'utf-8'));

  const { privateKeyBase58 } = signingKey;
  const signed = await issue({ credentialID, issuerKeys: { ...publicKey, ...signingKey }, publicKey, keyID: issuerKeyLoc(config.host, issuer), subject, schema, privateKeyBase58, config });
  return signed;
}

// issue a credential with the passed elements
export const issue = async ({ credentialID, issuerKeys, publicKey, keyID, subject, schema, privateKeyBase58, config }) => {
  publicKey.privateKeyBase58 = privateKeyBase58;

  const keyPair = new Ed25519KeyPair(issuerKeys);
  keyPair.id = keyID;

  const suite = new Ed25519Signature2018({
    verificationMethod: keyID,
    key: keyPair
  });

  const documentLoader = getDocumentLoader(config);
  delete subject['@context'];

  //  https://www.w3.org/TR/vc-data-model/#example-1-a-simple-example-of-a-verifiable-credential
  const credential = {
    '@context': [
      'https://www.w3.org/2018/credentials/v1',
      schemaInNS(config.host, schema)
    ],
    id: credentialID,
    type: ['VerifiableCredential', schema.replace(/\/.*/, '')],
    issuer: keyPair.controller,
    issuanceDate: getIso8601Date(),
    credentialSubject: subject
  }

  try {
    const result = await vc.issue({
      credential,
      suite,
      documentLoader
    });
    return result;
  } catch (e) {
    throw (e);
  }
};

