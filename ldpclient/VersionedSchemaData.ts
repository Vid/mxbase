import 'isomorphic-fetch';
import { getHostDataId } from '../lib/paths';
import LDPClient, { TResult } from '.';

export type TDataRows = Promise<{
    status: number,
    ok: boolean,
    rows?: any
}>

export class VersionedSchemaData {
    ctx: LDPClient;

    constructor(ctx) {
        this.ctx = ctx;
    }
    async post(schemaWithVersion: string, data: any): TResult {
        const response = await this.ctx.authn.post(`/app/data/${schemaWithVersion}`, { data });
        return response;
    }
    async get(schemaWithVersion: string, id?: string): TDataRows {
        const response = await this.ctx.authn.get(getHostDataId(this.ctx.host, schemaWithVersion, id));
        if (response.ok) {
            return { status: response.status, rows: response.result.rows, ok: response.ok };
        }
        return { status: response.status, ok: response.ok };
    }
}
