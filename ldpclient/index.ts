// https://www.w3.org/TR/ldp-primer/, https://www.w3.org/TR/ldp/#ldpdc
import fetch from 'isomorphic-fetch';

import { logger } from '../lib/logging';
import { JWTAuthn } from './JWTAuthn';
import { VersionedSchemaData } from './VersionedSchemaData';
import { VerifiableCredentials } from './VerifiableCredentials';
import { Doc } from './Doc';
import { isURI } from '../lib/paths';

export type TResult = Promise<{
    status: number,
    ok: boolean,
    result?: any,
    headers?: any,
    updated?: boolean
}>

export default class LDPClient {
    host: string;
    authn: JWTAuthn;
    data: VersionedSchemaData;
    vc: VerifiableCredentials;
    doc: Doc;
    logger;

    constructor(host: string) {
        this.host = host;
        this.authn = new JWTAuthn(this);
        this.data = new VersionedSchemaData(this);
        this.vc = new VerifiableCredentials(this);
        this.doc = new Doc(this);
        this.logger = logger;
    }
    // json get without auth
    async get(path) {
        const where = isURI(path) ? path : `${this.host}${path}`;
        const response = await fetch(where, {
            headers: { 'Content-Type': 'application/json' }
        });
        return response;
    }
    errorStatus(error, status = 500) {
        return { status, error };
    }
}

