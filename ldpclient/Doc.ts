import LDPClient from '.';
import { a } from '../lib/paths';

export class Doc {
    ctx: LDPClient;
    constructor(ctx) {
        this.ctx = ctx;
    }
    async get(path: string, type: 'json' | 'text') {
        return await this.ctx.authn.get(a(path), type);
    }
    async post(path: string, contents: any, type = 'json') {
        if (!path.startsWith('/')) {
            this.ctx.logger.warn('not absolute path', path);
            throw (`not absolute path${path}`)
        }
        const response = await this.ctx.authn.post(`/a${path}`, { contents }, type);
        return response;
    }
    async options(path: string) {
    }
    async delete(path: string) {
    }
}