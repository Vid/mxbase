import fetch from 'isomorphic-fetch';

import { logger } from '../lib/logging';
import { issuerKeyLoc, isURI } from '../lib/paths';
import LDPClient, { TResult } from '.';
import { getContentType } from '../lib/constants';

export class JWTAuthn {
    ctx: LDPClient;
    access_token: string;

    constructor(ctx) {
        this.ctx = ctx;
    }

    async createAccount({ username, password, emailAddress }: { username: string; password: string; emailAddress: string; }) {
        try {
            const response = await fetch(`${this.ctx.host}/app/account/register`, {
                method: 'post',
                body: JSON.stringify({
                    username,
                    emailAddress,
                    password
                }),
                headers: { 'Content-Type': 'application/json' }
            });
            if (response.ok) {
                const result = await response.json();
                this.access_token = result.access_token;
                return true;
            }
            else {
                logger.warn('createAccount failed', response.status);
                return false;
            }
        } catch (error) {
            logger.warn('createAccount fetch failed', error);
            return false;

        }
    }
    isLoggedIn() {
        return !!this.access_token;
    }
    async login({ username, password }: { username: string; password: string; }) {
        try {
            const response = await fetch(`${this.ctx.host}/app/account/login`, {
                method: 'post',
                body: JSON.stringify({
                    username,
                    password
                }),
                headers: { 'Content-Type': 'application/json' }
            });
            if (response.status === 200) {
                const result = await response.json();
                this.access_token = result.access_token;
                return true;
            } else {
                logger.warn('login failed', response.status);
                return false;
            }
        } catch (error) {
            logger.warn('login fetch failed', error);
            return false;
        }

    }
    // get with auth_token
    async get(path, type: 'text' | 'json' = 'json'): TResult {
        const where = isURI(path) ? path : `${this.ctx.host}${path}`;
        const response = await fetch(where, {
            headers: {
                'Content-Type': getContentType(type),
                authorization: 'bearer ' + this.access_token
            }
        });
        if (response.ok) {
            return { ok: response.ok, result: await response[type](), status: response.status };
        }
        return { status: response.status, ok: response.ok };
    }
    // post with auth_token
    async post(path: string, contents: any, type = 'json'): TResult {
        const response = await fetch(`${this.ctx.host}${path}`, {
            method: 'post',
            body: JSON.stringify(contents),
            headers: {
                'Content-Type': getContentType(type),
                authorization: 'bearer ' + this.access_token
            }
        });
        if (response.ok) {
            return { ...await response.json(), status: response.status, ok: response.ok, headers: response.headers };
        }
        return { status: response.status, ok: response.ok };
    }
    async logout() {
        await this.post(`/app/account/logout`, ({} as JSON));
        this.access_token = null;
        return true;
    }
    async getPublicKey(username: string) {
        const issuerID = issuerKeyLoc(this.ctx.host, username);
        const response = await this.ctx.get(issuerID);
        return { result: (response.ok && await response.json()), status: response.status };
    }
}
