import { verifyCredential } from '../vc';
import { getHostDataId } from '../lib/paths';

import LDPClient from '.';

export type TSigned = Promise<{
    status: number,
    ok: boolean,
    signed?: any
}>;

export class VerifiableCredentials {
    ctx: LDPClient;

    constructor(ctx) {
        this.ctx = ctx;
    }
    async verifyCredential(signed, config) {
        return await verifyCredential(signed, config);
    }
    async sign(schemaWithVersion, subject, documentID): TSigned {
        const response = await this.ctx.authn.post(`/app/sign`, { schema: schemaWithVersion, subject: { ...subject, id: getHostDataId(this.ctx.host, 'verifiableCredential/v1', documentID) } });
        return response;
    }
}
