import rimraf from 'rimraf';

import { mxbase, initSite } from './mxbase';
import { ldp } from './ldpserver';
import { accounts, keys, schema } from './extensions/';
import { tests as baseTests, TTestCTX } from './test/int/mxbase.int';
import { tests as vcTests } from './test/int/vc.int';
import { tests as ldpTests } from './test/int/ldp.int';
import { tests as editingTests } from './test/int/editing.int';
import LDPClient from './ldpclient';
import { containerDef } from './lib/constants';
const username = 'testissuer' + Date.now();
const schemaWithVersion = 'vc-document/v1';
const subject = {
    seen: 'True'
}

type TMXServer = any;
type TDone = () => void;

const host = 'http://localhost:2010';
let db;
let server: TMXServer;

const site_base = './test-out/int/site';
const config = {
    host,
    site_base,
    schemas: {
        ...containerDef,
        [schemaWithVersion]: {
            schema: {
                '@context': [
                    {
                        seen: 'http://schema.org/Boolean',
                    }
                ]
            }
        }
    }
}

const ctx: TTestCTX = {
    getDB: () => db,
    ldpClient: new LDPClient(config.host),
    config,
    v: { name: 'localhost', username, password: 'pasS!' + Date.now(), emailAddress: `${username}@nicer.info`, schemaWithVersion, subject }
};

const beforeAll = (async (done: TDone) => {
    rimraf.sync(site_base);
    db = await initSite({ host, site_base, jwtText: Math.random().toString(), db: ':memory:' });
    const startedCallback = async (serverIn: TMXServer) => {
        server = serverIn;
        done();
    }

    mxbase({ port: 2010, extensions: { ldp, keys, accounts, schema }, dbIn: db, getConfig: () => config, startedCallback });
});
const afterAll = (async (done: TDone) => {
    rimraf.sync(site_base);
    await db.close();
    server.close();
    done();
});

const tests = [
    baseTests,
    ldpTests,
    vcTests,
    editingTests,
];

// run the tests, with beforeAll and afterAll in the right places
for (let i = 0; i < tests.length; i++) {
    const t = tests[i];
    const options: { beforeAll?: any, afterAll?: any } = i === 0 ? { beforeAll } : {};
    if (i === tests.length - 1) {
        options.afterAll = afterAll;
    }
    t(ctx, options);
}
