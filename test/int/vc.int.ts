import { getHostDataId } from '../../lib/paths';
import { TTestCTX } from './mxbase.int';

export const tests = (ctx: TTestCTX, setup) => {
    const { ldpClient, config, v } = ctx;
    describe(`verifiable credentials`, () => {
        beforeAll(async (done) => {
            setup.beforeAll ? await setup.beforeAll(done) : done();
        });
        afterAll(async (done) => {
            setup.afterAll ? await setup.afterAll(done) : done();
        });
        it('creates a document', async () => {
            const response = await ldpClient.data.post(v.schemaWithVersion, v.subject);
            expect(response.status).toBe(201);
            expect(response).toBeDefined();
            expect(response.result).toBeDefined();
            v.documentID = response.result.id;
            expect(v.documentID).toBeGreaterThan(0);
        });
        it('retrieves all documents', async () => {
            const response = await ldpClient.data.get(v.schemaWithVersion);
            expect(response.status).toBe(200);
            const { rows } = response;
            expect(rows.length).toBe(1);
        });
        it('retrieves the document', async () => {
            const response = await ldpClient.data.get(v.schemaWithVersion, v.documentID);
            expect(response.status).toBe(200);
            const { rows } = response;
            expect(rows.length).toBe(1);
            const result = rows[0];
            expect(result.data).toBeDefined();
            expect(result.data.id).toBeDefined();
            expect({ ...result.data, '@context': undefined, id: undefined }).toEqual(v.subject);
            const retrievedID = result.data.id;
            expect(retrievedID).toBe(getHostDataId(config.host, v.schemaWithVersion, v.documentID));
        });
        it('creates a verifiable credential from the document', async () => {
            const result = await ldpClient.vc.sign(v.schemaWithVersion, v.subject, v.documentID);
            expect(result.status).toBe(200);
            v.signed = result.signed;
            expect(v.signed.proof).toBeDefined();
            v.vcID = parseInt(v.signed.id.replace(/.*\//, ''), 0);
            expect(v.vcID).toBeGreaterThan(v.documentID);
        });
        it('retrieves the verifiable credential', async () => {
            const response = await ldpClient.data.get('verifiableCredential/v1', v.vcID);
            expect(response.status).toBe(200);
            const { rows } = response;
            expect(rows.length).toBe(1);
            const result = rows[0];
            expect(result.data.id).toBe(getHostDataId(config.host, 'verifiableCredential/v1', v.vcID));
        });
        it('verifies the verifiable credential', async () => {
            const result = await ldpClient.vc.verifyCredential(v.signed, config);
            expect(result).toBeDefined();
            expect(result.error).toBe(undefined)
            expect(result.verified).toBe(true);
        })
    })
};
