import LDPClient from '../../ldpclient';
import { TConfig } from '../../mxbase';
import { DB } from '../../lib/db';

export type TTestCTX = { ldpClient: LDPClient, config: TConfig, v: { [name: string]: any }, getDB: () => DB };

export const tests = (ctx: TTestCTX, setup) => {
    const { ldpClient, config, v } = ctx;
    describe(`mxbase integration for ${v.name}`, () => {
        beforeAll(async (done) => {
            setup.beforeAll ? await setup.beforeAll(done) : done();
        });
        afterAll(async (done) => {
            setup.afterAll ? await setup.afterAll(done) : done();
        });
        it('creates an account', async () => {
            const creds = { username: v.username, password: v.password, emailAddress: v.emailAddress };
            const res = await ldpClient.authn.createAccount(creds);
            expect(ldpClient.authn.isLoggedIn()).toBe(true);
        });
        it('finds the account public key', async () => {
            const response = await ldpClient.authn.getPublicKey(v.username);
            expect(response.status).toBe(200);
            expect(response.result.id).toBeDefined();
        });
        it('logs out', async () => {
            const res = await ldpClient.authn.logout();
            expect(res).toBe(true);
            expect(ldpClient.authn.isLoggedIn()).toBe(false);
        });
        it('logs in', async () => {
            await ldpClient.authn.login({ username: v.username, password: v.password });
            expect(ldpClient.authn.isLoggedIn()).toBe(true);
        });
    });
};
