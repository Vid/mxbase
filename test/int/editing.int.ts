import { TTestCTX } from './mxbase.int';
import LDPClient from '../../ldpclient';

export const tests = (ctx: TTestCTX, setup) => {
    const { ldpClient, config, v } = ctx;
    describe(`editing data`, () => {
        let editor1 = new LDPClient(config.host);
        beforeAll(async (done) => {
            if (setup.beforeAll) {
                await setup.beforeAll();
            }
            const creds = { username: 'editor1', password: 'editor1Password', emailAddress: 'editor1@nicer.info' };
            await editor1.authn.createAccount(creds);
            await editor1.authn.login({ username: creds.username, password: creds.password });
            if (!editor1.authn.isLoggedIn()) {
                throw Error(`didn't create editor1`);
            }
            done();
        });
        afterAll(async (done) => {
            setup.afterAll ? await setup.afterAll(done) : done();
        });
        describe('basic save of markdown document', () => {
            const mdDoc = `
[:hasprop](wtw)

https://cbc.ca/news
        `;
            it('editors watch a query', () => {
            });
            it('saves editor1 markdown doc with a reminder', async () => {
                const res = await editor1.doc.post('/test/doc', mdDoc);
                expect(res.ok).toBe(true);
            });
            it('notifies editors of change', () => {
            });
            it('can query by date', async () => {

            });
            it('can query by container', async () => {
            });
        });
    });
}
