import { TTestCTX } from './mxbase.int';
import { a, getHostPageId, schemaInNS } from '../../lib/paths';

export const tests = (ctx: TTestCTX, setup) => {
    const { ldpClient, config, v, getDB } = ctx;
    describe(`linked data platform`, () => {
        const docLoc = '/test/doc';
        const mdDoc = `
[:hasprop](wtw)
`;
        beforeAll(async (done) => {
            setup.beforeAll ? await setup.beforeAll(done) : done();
        });
        afterAll(async (done) => {
            setup.afterAll ? await setup.afterAll(done) : done();
        });
        xit('container OPTIONS', async () => {
            // 5.2.3.13 must include an Accept-Post response header on HTTP OPTIONS responses, listing POST request media type(s) supported by the server. 
            expect(true).toBe(true);
        });
        xit('create a container', async () => {
            expect(true).toBe(true);
        });
        xit('verifies new container', async () => {
            expect(true).toBe(true);
        });
        // see 5.2.3.13
        //  must respond with status code 201 (Created) and the Location header set to the new resource’s URL. Clients shall not expect any representation in the response entity body on a 201 (Created) response.
        it('creates a markdown document', async () => {
            const res = await ldpClient.doc.post(docLoc, mdDoc);
            expect(res.ok).toBe(true);
            expect(res.status).toBe(201);
            expect(res.updated).toBe(false);
            // get is node-fetch specific
            expect(res.headers.get('location')).toBe(a(docLoc));
        });
        it('assigned a container path for data', async () => {
            const res = await getDB().findExpandedRows({ graph: getHostPageId(config.host, docLoc) }, { username: v.username, apex: false });
            const t = res.find(r => r.predicate === schemaInNS(config.host, 'container/v1/member') && !!r.apex);
            expect(t).toBeDefined();
            expect(t.predicate).toEqual(schemaInNS(config.host, 'container/v1/member'));
            expect(t.value_Text).toEqual('test');

        });
        expect(true).toBe(true);
        xit('verifies new RDF resource', async () => {
            expect(true).toBe(true);
        });
        xit('create a non-RDF resource', async () => {
            expect(true).toBe(true);
        });
        xit('verify a non-RDF resource', async () => {
            expect(true).toBe(true);
        });
        xit('update an RDF resource', async () => {
            expect(true).toBe(true);
        });
        xit('verifies updated RDF resource', async () => {
            expect(true).toBe(true);
        });
        xit('container GET', async () => {
            // TODO MUST respond with turtle for text/turtle or when Accept is absent
            // TODO MUST respond for application/ld+json
        });
        it('container GET for markdown', async () => {
            const res = await ldpClient.doc.get(docLoc, 'text')
            expect(res.result).toEqual(mdDoc);
        });
        xit('update a non-RDF resource', async () => {
            expect(true).toBe(true);
        });
        xit('verify updated non-RDF resource', async () => {
            expect(true).toBe(true);
        });
        xit('delete an RDF resource', async () => {
            expect(true).toBe(true);
        });
        xit('verifies deleted RDF resource', async () => {
            expect(true).toBe(true);
        });
        xit('delete a non-RDF resource', async () => {
            expect(true).toBe(true);
        });
        xit('verify deleted non-RDF resource', async () => {
            expect(true).toBe(true);
        });
        // https://github.com/samvera-deprecated/hydra/wiki/LDP-Containers-for-the-perplexed
        xit('create an Direct Container RDF resource', async () => {
            expect(true).toBe(true);
        });
        xit('verifies Direct new Container RDF resource', async () => {
            expect(true).toBe(true);
        });
    });
}
