
import { DB } from './db';
import { withSchema, getHostDataId } from './paths';

const username = 'testUser';
const schemaName = 'testSchema';
const version = 'v1';
const schema = `${schemaName}/${version}`;
const data = { value: 1 };
const updatedData = { value: 2 };
const host = 'https://test';

const withoutDates = (res) => Object.keys(res).reduce((all, a) => ['created', 'updated'].includes(a) ? all : { ...all, [a]: res[a] }, {});

describe('db', () => {
    let db;
    beforeAll(async () => {
        db = new DB(':memory:', host);
        await db.createTables();
    });
    afterAll(async () => {
        db.close();
    });
    let firstID;
    let secondID;
    let firstUpdated;

    it('gets IDs', async () => {
        const { id, graph } = await db.getID({ username, schema });
        firstID = id;
        expect(typeof id).toBe('number');
        expect(graph).toEqual(getHostDataId(host, schema, 1));
    });
    it('increments IDs', async () => {
        const { id, graph } = await db.getID({ username, schema });
        expect(firstID < id).toBe(true);
    });
    it('saves new data', async () => {
        const res = await db.saveData({ username, schema, data, host });
        firstID = res.id;
        expect(typeof firstID).toBe('number');
    });
    it('finds data by id', async () => {
        const rows = await db.findData({ schema, username, id: firstID });
        expect(Array.isArray(rows)).toBe(true);
        expect(rows.length).toBe(1);
        const res = rows[0];
        expect(typeof res.created).toBe('string');
        expect(typeof res.updated).toBe('string');
        firstUpdated = res.updated;

        expect(res.created).toEqual(res.updated);
        expect(withoutDates(res)).toEqual({ username, schema, data: withSchema({ data, id: firstID, schema, host }), id: firstID })
    })
    it('updates data', async () => {
        const res = await db.saveData({ username, schema, data: updatedData, id: firstID, host });
        expect(res).toBeDefined();
    });
    it('finds updated data by id', async () => {
        const rows = await db.findData({ schema, username, id: firstID });
        expect(Array.isArray(rows)).toBe(true);
        expect(rows.length).toBe(1);
        const res = rows[0];
        expect(res.updated > firstUpdated).toBeTruthy();
        expect(withoutDates(res)).toEqual({ username, schema, id: firstID, data: withSchema({ data: updatedData, id: firstID, schema, host }) });
    });
    it('finds data by schema', async () => {
        const res = await db.findData({ schema, username });
        expect(Array.isArray(res)).toBe(true);
        expect(res.length).toBe(1);
        expect(withoutDates(res[0])).toEqual({ id: firstID, username, schema, data: withSchema({ data: updatedData, id: firstID, schema, host }) })
    });
    it('saves more data', async () => {
        const res = await db.saveData({ username, schema, data, host });
        secondID = res.id;
        expect(typeof firstID).toBe('number');
    });
    it('finds more data by schema', async () => {
        const res = await db.findData({ schema, username });
        expect(typeof res).toBe('object');
        expect(res.length).toBe(2);
        expect(withoutDates(res[0])).toEqual({ id: firstID, username, schema, data: withSchema({ data: updatedData, id: firstID, schema, host }) })
        expect(withoutDates(res[1])).toEqual({ id: secondID, username, schema, data: withSchema({ data, id: secondID, schema, host }) })
    });
});