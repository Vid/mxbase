import rimraf from 'rimraf';
import jsonld from 'jsonld';

import * as data from './data';
import { mxbase, initSite } from '../mxbase';
import { schema } from '../extensions/';
import { getHostDataId, schemaInNS } from './paths';
import { DB } from './db';
import { FileCachingNetworkContextRetriever } from './data';

const host = 'http://localhost:4010';

const visitedSchemaWithVersion = `visited_link/v1`
const dataSchemaWithVersion = 'data/v1';
const hostSchema = schemaInNS(host, visitedSchemaWithVersion);
const site_base = './test-out/data/site';

const config = {
    host,
    site_base,
    schemas: {
        [dataSchemaWithVersion]: {
            schema: {
                '@context': {
                    uri: 'https://schema.org/URL',
                    title: 'https://schema.org/Text',
                }
            }
        },
        [visitedSchemaWithVersion]: {
            schema: {
                '@context': {
                    uri: 'https://schema.org/URL',
                    title: 'https://schema.org/Text',
                    visitor: 'https://schema.org/Person',
                    accessed: 'https://schema.org/Date'
                }
            }
        }
    }
}
const known = {
    [hostSchema]: {
        "@context": {
            "uri": {
                "@id": "https://schema.org/URL"
            },
            "title": {
                "@id": "https://schema.org/Text"
            },
            visitor: {
                '@id': 'https://schema.org/Person'
            },
            "accessed": {
                "@id": "https://schema.org/Date"
            }
        }
    }
}
class mockRetriever extends FileCachingNetworkContextRetriever {
    constructor(dontcare, stilldontcare) {
        super('a', 'b');
    }
    async get(uri) {
        return known[uri];
    }
}

const dataLink = {
    "@context": [hostSchema],
    "uri": "https://bbc.com/news",
    "title": "BBC news",
}

const testURI = 'https://cbc.ca/news';
const dataVisitedLink = {
    "@context": hostSchema,
    "uri": testURI,
    "title": "CBC news",
    "visitor": "https://app/profile/1",
    "accessed": data.getIso8601Date(),
}

const updatedVisitedLink = { ...dataVisitedLink, visitor: 'https://app/profile/2' };

const vcContext = ["https://www.w3.org/2018/credentials/v1",
    "https://www.w3.org/2018/credentials/examples/v1",
    hostSchema
];
const nestedSchema = {
    "@context": vcContext,
    "id": "http://example.com/credentials/1872",
    "type": ["VerifiableCredential", visitedSchemaWithVersion],
    "issuer": "https://example.com/issuers/565049",
    "issuanceDate": "2010-01-01T19:23:24Z",
    "credentialSubject": {
        "visitor": "https://app/profile/1",
        "accessed": data.getIso8601Date(),
        "id": `${host}/app/data/visited_link/v1/2`
    }
}

const dp = new data.DataProcessor(config.host, new mockRetriever(config.host, null));

describe('expandToRows', () => {
    it('fails if no @context', async (done) => {
        try {
            await dp.expandToRows({})
        } catch (e) {
            expect(e).toBeDefined();
            done();
        }
    });
    it('expandToRows schema data', async () => {
        const rows = await dp.expandToRows(dataVisitedLink);
        expect(rows).toBeDefined();

        const { apex, notApex, uri, person } = getFeatures(rows, dataVisitedLink);

        expect(apex.length).toBe(4);
        expect(notApex.length).toBe(11);
        expect(uri.length).toBe(4);
        expect(person.length).toBe(5);
    });
});

describe('integration tests', () => {
    let db: DB;
    let idid;
    let server;
    const username = 'test';

    beforeAll(async (done: () => void) => {
        rimraf.sync(site_base);
        db = await initSite({ host, site_base, jwtText: Math.random().toString(), db: ':memory:' });
        const startedCallback = async (serverIn: any) => {
            server = serverIn;
            done();
        }
        mxbase({ port: 4010, extensions: { schema }, dbIn: db, getConfig: () => config, startedCallback });
    });
    afterAll(async (done: () => void) => {
        await db.close();
        server.close();
        rimraf.sync(site_base);
        done();
    });
    describe('basic saving and retrieving', () => {
        it('saves expanded data', async () => {
            const res = await db.saveExpandedRows(dp.expandToRows(dataVisitedLink), { username, schema: visitedSchemaWithVersion });
            idid = res.idid;
            expect(idid).toBe(1);
        });
        it('retrieves apex expanded rows', async () => {
            const re = await db.findExpandedRows({ id: idid }, { username, apex: true });
            const exp = data.defined(re);
            expect(exp).toBeDefined();
            expect(exp.length).toEqual(5);
            expect(exp.every(e => !!e.graph)).toBe(true);
            expect(exp.every(e => !!e.subject)).toBe(true);
            expect(exp.every(e => !!e.isType)).toBe(true);
            expect(exp.every(e => !!e.isType)).toBeDefined();
        });
        it('retrieves apex expanded count', async () => {
            const re = await db.findExpandedRows({ id: idid }, { username, apex: true });
            const exp = data.defined(re);
            expect(exp).toBeDefined();
            expect(exp.length).toEqual(5);
            expect(exp.every(e => !!e.graph)).toBe(true);
            expect(exp.every(e => !!e.subject)).toBe(true);
            expect(exp.every(e => !!e.isType)).toBe(true);
            expect(exp.every(e => !!e.isType)).toBeDefined();
        });
        it('updates graph', async () => {
            const graph = getHostDataId(host, visitedSchemaWithVersion, idid);
            const res = await db.saveExpandedRows(dp.expandToRows(updatedVisitedLink), { username, schema: visitedSchemaWithVersion, graph });
            expect(res.idid).toBe(1);
            expect(res.updated).toBe(true);
        });
    });
    describe('results compatible with jsonld-js', () => {
        it('has basic compatibility', async () => {
            const expanded = await jsonld.expand(dataVisitedLink);
            const compacted = await jsonld.compact(expanded, hostSchema)
            // FIXME should be able to include id
            expect(compacted).toEqual({ ...dataVisitedLink, id: undefined })
        });
        it('gets the same result for jsonld expand and expandRows', async () => {
            const re = await db.findExpandedRows({ id: idid }, { username, apex: true });
            const de = await data.expandRows(re);

            const je = await jsonld.expand(updatedVisitedLink);
            expect(je).toEqual(de);
        });
        it('handles nested objects', async () => {
            const jexpanded = await jsonld.expand(nestedSchema);
            const jcompacted = await jsonld.compact(jexpanded, vcContext)
            expect(jcompacted).toEqual(nestedSchema)
        });
        it('thinks about rdf form', async () => {
            const context = ["https://www.w3.org/2018/credentials/v1",
                "https://www.w3.org/2018/credentials/examples/v1",
                hostSchema
            ];
            const jrdf = await jsonld.toRDF(nestedSchema);
            const jfrom = await jsonld.fromRDF(jrdf);
            const jrcompacted = await jsonld.compact(jfrom, context);
            // dbg({ jrdf, jfrom, jrcompacted });
            expect(jrcompacted).toBeDefined();
        });
    });
    describe('assigned id', () => {
        it('retrieves expanded rows', async () => {
            const re = await db.findExpandedRows({ id: idid }, { username });
            const exp = data.defined(re);
            expect(exp.length).toBe(16);
        });
        it('retrieves defined data', async () => {
            const re = await db.findExpandedRows({ id: idid }, { username });
            const exp = data.defined(re);
            const { apex, notApex, uri, person } = getFeatures(exp, updatedVisitedLink);
            expect(apex.length).toBe(5);
            expect(notApex.length).toBe(11);
            expect(uri.length).toBe(4);
            expect(person.length).toBe(5);
        });
        it('retrieves defined apex data', async () => {
            const re = await db.findExpandedRows({ id: idid }, { username, apex: true });
            const exp = data.defined(re);
            const { apex, notApex, uri, person } = getFeatures(exp, updatedVisitedLink);
            expect(apex.length).toBe(5);
            expect(notApex.length).toBe(0);
            expect(uri.length).toBe(1);
            expect(person.length).toBe(1);
        });
        it('retrieves and compacts data', async () => {
            const re = await db.findExpandedRows({ id: idid }, { username, apex: true });
            const exp = await data.compactRows(re);
            expect(exp).toEqual({ ...updatedVisitedLink, id: undefined });
        });
        it('retrieves and compacts by subject', async () => {
            const re = await db.findExpandedRows({ subject: getHostDataId(host, visitedSchemaWithVersion, 1) }, { username, apex: true });
            const exp = data.defined(re);
            const { apex, notApex, uri, person } = getFeatures(exp, updatedVisitedLink);
            expect(apex.length).toBe(5);
            expect(notApex.length).toBe(0);
            expect(uri.length).toBe(1);
            expect(person.length).toBe(1);
        });
        it('retrieves and compacts by object', async () => {
            const re = await db.findExpandedRows({ object: testURI }, { username, apex: true });
            const exp = data.defined(re);
            const { apex, notApex, uri, person } = getFeatures(exp, updatedVisitedLink);
            expect(apex.length).toBe(5);
            expect(notApex.length).toBe(0);
            expect(uri.length).toBe(1);
            expect(person.length).toBe(1);
        });
        it('retrieves by subject, predicate and object', async () => {
            const re = await db.findExpandedRows({ subject: getHostDataId(host, visitedSchemaWithVersion, 1), predicate: hostSchema + '/uri', object: testURI }, { username, apex: true });
            const exp = data.defined(re);
            const { apex, notApex, uri, person } = getFeatures(exp, updatedVisitedLink);
            expect(apex.length).toBe(5);
            expect(notApex.length).toBe(0);
            expect(uri.length).toBe(1);
            expect(person.length).toBe(1);
        });
        it('retrieves count by subject, predicate and object', async () => {
            const re = await db.getExpandedCount({ subject: getHostDataId(host, visitedSchemaWithVersion, 1), predicate: hostSchema + '/uri', object: testURI }, { username, apex: true });
            expect(re).toBe(5);
        });
        it('retrieves and compacts by object with type', async () => {
            const re = await db.findExpandedRows({ object: testURI }, { username, apex: true });
            const exp = data.defined(re);
            const { apex, notApex, uri, person } = getFeatures(exp, updatedVisitedLink);
            expect(apex.length).toBe(5);
            expect(notApex.length).toBe(0);
            expect(uri.length).toBe(1);
            expect(person.length).toBe(1);
        });
        it('retrieves and compacts by graph', async () => {
            const re = await db.findExpandedRows({ graph: getHostDataId(host, visitedSchemaWithVersion, 1) }, { username, apex: true });
            const exp = data.defined(re);
            const { apex, notApex, uri, person } = getFeatures(exp, updatedVisitedLink);
            expect(apex.length).toBe(5);
            expect(notApex.length).toBe(0);
            expect(uri.length).toBe(1);
            expect(person.length).toBe(1);
        });
        it('retrieves and compacts by widened predicate', async () => {
            const predicate = 'https://schema.org/Person';
            await db.saveExpandedRows(dp.expandToRows(dataLink), { username, schema: dataSchemaWithVersion });
            const re = await db.findExpandedRows({ predicate }, { username, apex: true });
            const exp = data.defined(re);
            const { apex, notApex, uri, person } = getFeatures(exp, updatedVisitedLink);
            expect(apex.length).toBe(5);
            expect(notApex.length).toBe(0);
            expect(uri.length).toBe(1);
            expect(person.length).toBe(1);
        });
        it('retrieves widened predicate from mix of data', async () => {
            await db.saveExpandedRows(dp.expandToRows(updatedVisitedLink), { username, schema: visitedSchemaWithVersion })
            await db.saveExpandedRows(dp.expandToRows(dataLink), { username, schema: dataSchemaWithVersion });

            const re = await db.findExpandedRows({ predicate: 'https://schema.org/Person' }, { username, apex: true });
            const exp = data.defined(re);
            const { apex, notApex, uri, person } = getFeatures(exp, updatedVisitedLink);
            expect(apex.length).toBe(10);
            expect(notApex.length).toBe(0);
            expect(uri.length).toBe(2);
            expect(person.length).toBe(2);
        });
        it('retrieves and compacts by subject from mix', async () => {
            const re = await db.findExpandedRows({ subject: getHostDataId(host, visitedSchemaWithVersion, 1) }, { username, apex: true });
            const exp = data.defined(re);
            const { apex, notApex, uri, person } = getFeatures(exp, updatedVisitedLink);
            expect(apex.length).toBe(5);
            expect(notApex.length).toBe(0);
            expect(uri.length).toBe(1);
            expect(person.length).toBe(1);
        });
        it('retrieves all with testURI object', async () => {
            const re = await db.findExpandedRows({ object: testURI }, { username, apex: false });
            const exp = data.defined(re);
            const bg = data.byGraph(exp);
            expect(Object.keys(bg).length).toBe(2);
            const a = bg[getHostDataId(host, visitedSchemaWithVersion, 1)];
            expect(a).toBeDefined();
            const { apex, notApex, uri, person } = getFeatures(a, updatedVisitedLink);
            expect(apex.length).toBe(5);
            expect(notApex.length).toBe(11);
            expect(uri.length).toBe(4);
            expect(person.length).toBe(5);
        });
    });
    // NB should this ever be used?
    describe('passed subject', () => {
        const testSubject = 'https://wtw';
        let subjectIDID
        it('saves data with subject', async () => {
            const res = await db.saveExpandedRows(dp.expandToRows({ ...dataVisitedLink, id: testSubject }), { username, schema: visitedSchemaWithVersion });
            subjectIDID = res.idid;
            expect(subjectIDID).toBe(5);
        });
        it('retrieves data with subject', async () => {
            const re = await db.findExpandedRows({ id: subjectIDID }, { username, apex: true });
            const exp = data.defined(re);
            expect(exp).toBeDefined();
            expect(exp.length).toEqual(6);
            const subject = exp.find(e => e.isType === 'id');
            expect(subject).toBeDefined();
            expect(subject.subject).toEqual(testSubject);
            expect(exp.every(e => !!e.graph)).toBe(true);
            expect(exp.every(e => !!e.subject)).toBe(true);
            expect(exp.every(e => !!e.isType)).toBe(true);
            expect(exp.every(e => !!e.isType)).toBeDefined();
        });
    })
});

function getFeatures(rows, cmp) {
    const apex = rows.filter(r => r.apex);
    const notApex = rows.filter(r => !r.apex);
    const uri = rows.filter(r => Object.entries(r).find(([k, v]) => k.startsWith('value_') && v === cmp.uri));
    const person = rows.filter(r => Object.entries(r).find(([k, v]) => k.startsWith('value_') && v === cmp.visitor));
    return { apex, notApex, uri, person };
}
