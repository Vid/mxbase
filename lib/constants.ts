
export const idField = 'id';
export const APP_NS = '/app/ns';

export interface ILink {
    title: string;
    url: string;
    text: string;
}
export interface IPageMeta {
    source?: string;
    sourced?: string;
    mdast: any;
    links: ILink[];
    klasses: ILink[];
    error?: any;
    origin?: string;
    changed: boolean;
    saved?: string;
    type: string;
    typeData?: any;
}
export type PageMap = { [name: string]: IPageMeta };
export type StringMap = { [name: string]: string };

export const TYPES: StringMap = {
    folder: 'folder',
    text: 'text/plain',
    markdown: 'text/markdown',
    turtle: 'text/turtle',
    jsonld: 'application/ld+json'
}

export const COLLECTION = 'Collection';
export const IS_REQUIRED = ':is required';
export const USES_SCHEMA = ':uses schema';
export const HAS_FIELD = ':has field';

export const getContentType = (type) => type === 'json' ? 'application/json' : 'text/plain';

export const containerSchemaName = 'container/v1';

export const containerDef = {
    [containerSchemaName]: {
        schema: {
            "@context": {
                "member": {
                    "@id": "http://www.w3.org/ns/ldp#member",
                    "@type": "@id"
                },
                "title": "http://purl.org/dc/terms/title",
                "dcterms": "http://purl.org/dc/terms/",
                "ldp": "http://www.w3.org/ns/ldp#",
                "rdfs": "http://www.w3.org/2000/01/rdf-schema#"
            }
        }
    }
}