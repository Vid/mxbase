import PizZip from 'pizzip';
import Docxtemplater from 'docxtemplater';
import * as fs from 'fs';
import * as path from 'path';

import { logger } from './logging';

export const generateDocx = (loc: string, template: string, fields: { [name: string]: string }) => {
    const input = [loc, template.replace(/[^a-zA-Z0-9_]/g, ''), '.docx'].join('');
    const wd = process.cwd();
    const content = fs.readFileSync(path.resolve(wd, input), 'binary');

    const zip = new PizZip(content);
    var doc = new Docxtemplater();
    doc.loadZip(zip);

    // FIXME get text value
    const fieldsAsText = Object.keys(fields).reduce((all, f) => f.endsWith(':text') ? { ...all, [f.replace(/:text/, '')]: fields[f] } : all, fields);
    //set the templateVariables
    doc.setData(fieldsAsText);

    try {
        doc.render()
    }
    catch (error) {
        const e = {
            message: error.message,
            name: error.name,
            stack: error.stack,
            properties: error.properties,
        }
        logger.error(JSON.stringify({ error: e }));
        // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
        throw error;
    }

    const buf = doc.getZip().generate({ type: 'nodebuffer' });
    return buf;
}


