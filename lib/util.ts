import fs from 'fs';
import path from 'path';
import mkdirp from 'mkdirp';

export async function mkpaths(site_base) {
  [site_base, `${site_base}/keys`, `${site_base}/private`, `${site_base}/private/signing`].forEach(dir => {
    if (!fs.existsSync(dir)) {
      mkdirp.sync(dir);
    }
  });
}

export function dbg(what) {
  console.log(JSON.stringify(what, null, 2));
}

// Accepts a path with a leading slash, removes any dangerous components
export function safePath(site_base: string, i: string) {
  const p = path.resolve(path.join(site_base, path.normalize(i)));
  return p.startsWith(path.resolve(site_base)) ? p : undefined;
}

export function assert(what) {
  Object.keys(what).forEach(k => {
    if (what[k] === undefined) {
      throw Error(`missing ${k}`);
    }
  });
}