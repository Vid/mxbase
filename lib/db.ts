import path from 'path';
import { Sequelize, Transaction } from 'sequelize';
import bcrypt from 'bcryptjs';

import { logger } from './logging';
import { withSchema, getHostDataId } from './paths';
import { TDBRow, getIdRow, valueFields } from './data';
import sequelize from 'sequelize';
import { assert } from './util';

type TData = {
    id: number,
    username: string,
    schema: string,
    created: Date,
    updated: Date,
    data: any
}

export class DB {
    database = undefined;
    host;
    constructor(databaseLocation, host) {
        this.database = new Sequelize({
            dialect: 'sqlite',
            storage: path.join(databaseLocation),
            logging: logger.debug.bind(logger)
        });
        this.host = host;
    }
    private async runStatement(sqlQuery, replacements, transaction?): Promise<{ results: any[], metadata }> {
        if (!Array.isArray(replacements)) {
            throw Error(`replacements passed as single ${replacements}`);
        }
        try {
            const [results, metadata] = await this.database.query(sqlQuery, { replacements, raw: true, type: sequelize.QueryTypes.RAW, transaction });
            return { results, metadata };
        } catch (e) {
            logger.error('query failed', e);
            throw (e);
        }
    }
    private async runQuery(sqlQuery, transaction?): Promise<{ results, metadata }> {
        try {
            const [results, metadata] = await this.database.query(sqlQuery, { raw: true, type: sequelize.QueryTypes.RAW, transaction });
            return { results, metadata };
        } catch (e) {
            logger.error('query failed', e);
            throw (e);
        }
    }

    async createTables() {
        await this.createUsersTable();
        await this.createExpandedRowsTable();
        await this.createDataTable();
        await this.createIDTable();
    }

    // test if the database is set up
    async isCreated() {
        try {
            const res = await this.runQuery('select * from users limit 1');
            return true;
        } catch (e) {
            if (e.message && e.message.includes('no such table: users')) {
                return false;
            }
            throw (e);
        }
    }
    close() {
        this.database.close();
    }
    private async getID({ graph, schema, username }: { graph?: string, schema: string, username: string }): Promise<{ id: number, graph: string }> {
        assert({ username });
        let id;
        if (graph) {
            id = await this.insertID({ username, graph });
            return { id, graph };
        } else {
            return await this.insertIDAndGraph({ username, schema });
        }
    }
    private async insertIDAndGraph({ username, schema }): Promise<{ id: number, graph: string }> {
        const transaction = await this.database.transaction({ autocommit: false });
        try {
            const id = await this.insertID({ username }, transaction);
            const graph = getHostDataId(this.host, schema, id);
            await this.updateID({ id, graph, username }, transaction);
            await transaction.commit();
            return { graph, id };
        } catch (e) {
            transaction.rollback();
            throw (e);
        }
    }
    private async updateID({ id, graph, username }, transaction?) {
        return await this.runStatement('UPDATE ids SET graph = ? WHERE username = ? AND id = ?', [graph, username, id], transaction)
    }
    private async insertID({ graph, username }: { graph?: string, username: string, transaction?: Transaction }, transaction?): Promise<number> {
        assert({ username });
        const now = new Date().toISOString();
        return (await this.runStatement(`INSERT INTO ids (graph, username, created) VALUES(?, ?, ?)`, [graph, username, now], transaction)).metadata.lastID;
    }

    private async findDataById({ schema, id, username }): Promise<TData[]> {
        const { query, p } = this.withDataUsername(`SELECT * FROM data 
            WHERE schema = ? and id = ?`, [schema, id], username);
        return (await this.runStatement(query, p)).results;
    }

    private async findDataByUsername(username): Promise<TData[]> {
        const query = `SELECT * FROM data WHERE username  = ?`;
        return (await this.runStatement(query, [username])).results;
    }
    private async findDataBySchema({ schema, username }) {
        const { query, p } = this.withDataUsername(`SELECT * FROM data WHERE schema = ?`, [schema], username);
        const results = (await this.runStatement(query, p));
        return results.results;
    }

    // FIXME temporary for gross data table
    private withDataUsername(query, params: any[], username) {
        let p = [...params];
        if (username) {
            query += ` and username = ?`;
            p = p.concat(username);
        }
        return { query, p };
    }
    private withUsername(query, params: any[], username) {
        let p = [...params];
        if (username) {
            query += ` and ids.username = ?`;
            p = p.concat(username);
        }
        return { query, p };
    }

    private async createIDTable() {
        for (let i of ['ids', 'pastIds']) {
            const sqlQuery = `
        CREATE TABLE IF NOT EXISTS ${i} (
        created date,
        username text,
        id integer ${i === 'ids' ? 'PRIMARY KEY' : ''},
        graph text)`;
            await this.runQuery(sqlQuery);
        }
    }
    private async createExpandedRowsTable() {
        for (let i of ['expanded', 'pastExpanded']) {
            const sqlQuery = `
        CREATE TABLE IF NOT EXISTS ${i} (
        seq integer ${i === 'expanded' ? 'PRIMARY KEY' : ''},
        idid integer,
        subject text,
        predicate text,
        isType text,
        value_Text string,
        value_Integer number,
        value_Float number,
        value_Date number,
        value_Time number,
        value_Boolean boolean,
        apex boolean)`;
            await this.runQuery(sqlQuery);
        }
    }
    private async createUsersTable() {
        const sqlQuery = `
        CREATE TABLE IF NOT EXISTS users (
        id integer PRIMARY KEY,
        username text UNIQUE,
        emailAddress text UNIQUE,
        password text)`;
        return await this.runQuery(sqlQuery);
    }
    private async createDataTable() {
        const sqlQuery = `
        CREATE TABLE IF NOT EXISTS data (
        id integer PRIMARY KEY,
        username text,
        created date,
        updated date,
        schema text,
        data json1)`;

        return await this.runQuery(sqlQuery);
    }

    async findData({ schema, id, username }: { schema, id?, username?}) {
        if (id === undefined) {
            return await this.findDataBySchema({ schema, username });
        }
        return await this.findDataById({ schema, id, username });
    }
    async deleteData({ schema, id, username }) {
        const { query, p } = this.withUsername(`DELETE FROM data WHERE schema = ? and id = ?`, [schema, id], username);
        await this.runStatement(query, p);
    }
    async getExpandedCount(input: { subject?: string, predicate?: string, object?: string }, options: { username: string, apex?: boolean }) {
        return (await this.expandedQuery('count(*)', input, options))[0]['count(*)'];
    }
    async findExpandedRows(input: { id?: number, subject?: string, predicate?: string, object?: string, graph?: string }, options: { username: string, apex?: boolean }) {
        return this.withGraph((await this.expandedQuery('DISTINCT e.*, ids.username', input, options)));
    }
    private async expandedQuery(what: string, { id, subject, predicate, object, graph }: { id?: number, subject?: string, predicate?: string, object?: string, graph?: string }, { username, apex }: { username: string, apex?: boolean }) {
        let wheres = [];
        let values = [];
        if (id) {
            wheres.push('i.id = ?');
            values.push(id);
        }
        if (graph) {
            wheres.push('ids.graph = ?');
            values.push(graph);
        }
        if (subject) {
            wheres.push('subject = ?');
            values.push(subject);
        }
        if (predicate) {
            wheres.push('predicate = ?');
            values.push(predicate);
        }
        if (object) {
            let owheres = [];
            const whereTypes = valueFields;
            owheres.push(whereTypes.map(f => `${f} = ?`).join(' OR '));
            wheres.push(`(${owheres})`);
            whereTypes.forEach(() => values.push(object));
        }
        if (wheres.length < 1) {
            return [];
        }
        const { query, p } = this.withUsername(`SELECT ${what} FROM expanded e, ids
             LEFT JOIN ids i ON e.idid = i.id 
             WHERE e.idid IN (SELECT distinct(idid) FROM expanded WHERE ${wheres.join(' AND ')})
            `
            + (apex ? ' AND apex = true' : '')
            , values, username);
        return (await this.runStatement(query, p)).results;
    }
    private async getGraphByID(id) {
        const res = await this.runStatement(`SELECT graph from ids where id = ?`, [id]);
        return res.results[0].graph
    }
    // FIXME re-add graph to query
    private async withGraph(rows) {
        const graphs = {};
        const graphed = [];
        for (let r of rows) {
            const idid = r.idid;
            let graph = graphs[idid];
            if (!graph) {
                graph = await this.getGraphByID(idid);
                graphs[idid] = graph;
            }
            graphed.push({ ...r, graph });
        }
        return graphed;
    }

    // Passing a promise to avoid forgetfullness
    async saveExpandedRows(rowsIn: Promise<TDBRow[]>, { username, schema, graph, idid }: { username: string, schema: string, graph?: string, idid?: number }) {
        assert({ username, schema });
        const rows = await rowsIn;
        if (!graph) {
            const { id: newID, graph: newGraph } = await this.getID({ graph, schema, username });
            idid = newID;
            const { subjectRow, rows: toInsert } = this.subjectify(rows, newGraph);
            await this.doSaveRows(idid, subjectRow, toInsert);
            return ({ idid });
        } else {
            const { subjectRow, rows: toInsert } = this.subjectify(rows, graph);
            const { results } = await this.runStatement(`SELECT id FROM ids WHERE graph = ?`, [graph]);
            if (!results || !results[0] || !results[0].id) {
                const { id: newID } = await this.getID({ graph, schema, username });
                idid = newID;
                // throw Error(`update for non existant graph ${graph}`);
            } else {
                idid = results[0].id;
            }
            const transaction = await this.database.transaction({ autocommit: false });
            await this.runStatement(`INSERT INTO pastIds SELECT * from ids WHERE id = ?`, [idid], transaction);
            await this.runStatement(`INSERT INTO pastExpanded SELECT * from expanded WHERE idid = ?`, [idid], transaction);
            await this.runStatement(`DELETE FROM expanded WHERE idid = ?`, [idid], transaction);
            await this.doSaveRows(idid, subjectRow, toInsert, transaction);
            transaction.commit();
            return ({ idid, updated: true });
        }
    }

    private subjectify(rows, graph) {
        let subjectRow = rows.find(r => r.isType === 'id');
        if (!subjectRow) {
            const subjectID = graph;
            subjectRow = getIdRow(subjectID);
            if (!subjectRow) {
                throw Error(`missing subject ${JSON.stringify(rows)}`);
            }
        }
        const subjectID = subjectRow.subject;
        rows = rows.map(r => ({ ...r, subject: subjectID }));
        return { subjectRow, rows };
    }

    // actually save the rows
    private async doSaveRows(idid, subject, rows, transaction?) {
        for (const row of [subject, ...rows]) {
            let fields = ['idid', 'subject'];
            let values = [idid, subject.subject];
            Object.keys(row).filter(field => field !== 'fieldID').forEach((field) => {
                fields.push(field);
                values.push(row[field]);
            });
            const query = 'INSERT INTO expanded (' + fields.join(', ') + ') VALUES(' + values.map(v => '?').join(', ') + ')';
            await this.runStatement(query, values, transaction);
        }
    }
    async saveData({ username, schema, id, data, host }: { username, schema, id?: number, data, host }) {
        assert({ schema, username });
        if (!id) {
            id = (await this.getID({ schema: 'data/v1', username })).id;
        }

        const jdata = withSchema({ data, id, schema, host });
        const now = new Date().toISOString();
        await this.runStatement(`INSERT INTO data (id, schema, username, data, created, updated) VALUES(?, ?, ?, ?, ?, ?) ON CONFLICT(id) DO UPDATE SET data=?, updated=?`,
            [id, schema, username, jdata, now, now, jdata, now]);
        return ({ id });
    }

    async findUserByUsername(username) {
        return (await this.runStatement(`SELECT * FROM users WHERE username = ?`, [username])).results[0];
    }
    async deleteByUsername(username) {
        return await this.runStatement(`DELETE FROM users WHERE username = ?`, [username]);
    }
    async createUser({ username, emailAddress, password }) {
        const hashed = bcrypt.hashSync(password);

        return await this.runStatement('INSERT INTO users (username, emailAddress, password) VALUES (?,?,?)', [username, emailAddress, hashed]);
    }
    async dump(table) {
        const rows = await this.runQuery(`SELECT * FROM ${table}`);
        console.log(`dump ${table}`, rows);
    }
}
