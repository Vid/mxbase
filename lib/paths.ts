import { idField } from './constants';
import { ILink } from './constants';

export const schemaInNS = (host, schema) => `${host}/app/ns/${schema}`;
export const issuerKeyBase = (host) => `${host}/app/key/`;
export const issuerKeyLoc = (host, username) => `${host}/app/key/${username}.json`;
export const withSchema = ({ data, id, schema, host }) => JSON.stringify({ '@context': [schemaInNS(host, schema)], ...data, [idField]: getHostDataId(host, schema, id) });
export const getHostDataId = (host, schema, id) => host + `/app/data/${schema}/` + (id !== undefined ? id : '');
export const a = (where: string) => isURI(where) ? where : ('/a/' + getFolder(where.replace(/^\//, ''))) + encodeURIComponent(getFile(where)!);
export const getHostPageId = (host: string, where: string) => isURI(where) ? where : (`${host}/a/` + getFolder(where.replace(/^\//, ''))) + encodeURIComponent(getFile(where)!);

export const hasKlassIntersection = (klasses: ILink[], cont: ILink[]) => { //boo
    return !!klasses.find((k: ILink) => cont.find((c: ILink) => k.url === c.url));
}

export const hasKlass = (klasses: ILink[], klass: string) => {
    return !!klasses.map(k => k.url).includes(klass);
}

export const isVirtual = (page: string) => page.includes('/(');
export const isURI = (what: string) => what && what.includes('://');


// FIXME following shouldn't be here
export const getRelativePathed = (ground: string, path: string) => {
    if (path.startsWith('/')) {
        return path;
    }
    const folder = getFolder(ground);
    const file = (getFile(path) || '').replace(/ /g, '_');

    return `${folder}${file}`;
}
export const getFolder = (location: string) => {
    if (!location) {
        console.error('no location');
        return '';
    }
    return location.substr(0, location.lastIndexOf('/') + 1);
}
// make a file relative to ground
export const getFile = (location: string) => {
    const res = location.substr(location.lastIndexOf('/') + 1);
    return res.length ? res : undefined;
}
export const resolveSchemaUri = (src: string, ground: string) => isURI(src) ? src : getRelativePathed(ground, src);

export function getSchemaVer(schema, version) {
    if (!schema || !version) {
        throw Error(`missing schema component "${schema}" "${version}"`);
    }
    return `${schema}/${version}`;
}