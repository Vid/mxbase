import * as paths from './paths';
describe('paths', () => {
    it('gets a folder', () => {
        expect(paths.getFolder('/hi/there')).toBe('/hi/');
        expect(paths.getFolder('/hi/there/')).toBe('/hi/there/');
    })
    it('gets a file', () => {
        expect(paths.getFile('/hi/there')).toBe('there');
        expect(paths.getFile('/hi/there/')).toBe(undefined);
        expect(paths.getFile('/hi/there/eh')).toBe('eh');
    })
});
describe('hasKlassIntersection', () => {
    const title = 'testtitle';
    const text = 'testtext';
    it('finds an intersection', () => {
        expect(paths.hasKlassIntersection([{ url: 'A', title, text }, { url: 'B', title, text }], [{ url: 'A', title, text }])).toEqual(true);
    });
    it('finds no intersection', () => {
        expect(paths.hasKlassIntersection([{ url: 'A', title, text }, { url: 'B', title, text }], [{ url: 'C', title, text }])).toBe(false);
    });

})
