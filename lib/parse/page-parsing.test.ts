
import * as pageParsing from './page-parsing';
import { TYPES, IPageMeta } from '../constants';
// import { RdfStore, AQuery } from './rdfstore';
const { markdown } = TYPES;

class RdfStore {
  storePage(pageName, page) { }
  propQuery(what) { return { rows: [] } }
}
class AQuery {
  constructor(name, params) {
  }
}

const everything = new AQuery('Everything', {});
const PM = { mdast: '', klasses: [], links: [], changed: false };
const markdownWithCollection = `
[:inKlass](Collection)
[:inKlass](AllKlass)

# Item1
[A link](Item_2)
[:inKlass](aKlass)

# Item 2
[A link](Elsewhere)
[Another link](Otherwhere)
[:inKlass](bKlass)
`;

describe('getOutputs', () => {
  it('gets collections', () => {
    const res = pageParsing.getOutputs('test', markdownWithCollection, markdown);
    expect(res).toBeDefined();
    expect(Object.keys(res!).sort()).toEqual(['test', 'Item1', 'Item_2'].sort());
    expect(res!.test.links).toEqual([])
    expect(res!.Item1.links).toEqual([{ "text": "A link", "title": null, "url": "Item_2" }])
    expect(res!.Item1.klasses).toEqual([{ "text": "inKlass", "title": null, "url": "aKlass" }, { "text": "inKlass", "title": null, "url": "AllKlass" }])
    expect(res!.Item_2.links).toEqual([{ "text": "A link", "title": null, "url": "Elsewhere" }, { "text": "Another link", "title": null, "url": "Otherwhere" }])
    expect(res!.Item_2.klasses).toEqual([{ "text": "inKlass", "title": null, "url": "bKlass" }, { "text": "inKlass", "title": null, "url": "AllKlass" }])
  });
});


describe('updatePages', () => {
  it('adds links', () => {
    // const rdfstore = new RdfStore();
    const rdfstore = new RdfStore();

    const res = pageParsing.pagesFromJSON('/test/', {
      test1: {
        ...PM,
        type: markdown,
        source: `
[:](Skill_Claim_Not_Accepted)
[:](Skill_Testing)

[:inKlass](Database)
`
      }
    }, (pageName: string, page: IPageMeta) => rdfstore.storePage(pageName, page));
    const test1 = res['/test/test1'];

    expect(test1).toBeDefined();

    expect(test1.links).toMatchObject([{ title: null, url: '/test/Skill_Claim_Not_Accepted', text: 'blank link' }, { title: null, url: '/test/Skill_Testing', text: 'blank link' }])
    expect(test1.klasses).toMatchObject([{ title: null, url: 'Database', text: 'inKlass' }])
  });
});

describe('links', () => {
  it('parses links', async () => {
    const res = pageParsing.getMarkdownOutputs('test', `\n[testtext](nospaces)\n`);
    expect(res.test).toBeDefined();
    expect(res.test.links.length).toBe(1);
  })
  it('gets link uris with spaces', async () => {
    const res = pageParsing.getMarkdownOutputs('test', `[testtext](with spaces)`);
    expect(res.test).toBeDefined();
    expect(res.test.links.length).toBe(1);
  })
  xit('parses turtle in links', async () => {
    const rdfstore = new RdfStore();
    const pages = {
      t1: { type: markdown, source: `\n[:t1;seq=1](t2)\n`, ...PM }
    }
    pageParsing.pagesFromJSON('/test/', pages, (pageName: string, page: IPageMeta) => rdfstore.storePage(pageName, page));

    const results = await rdfstore.propQuery(everything);
    expect(results.rows.length).toBe(2);

  })
});

describe('finds schemas', () => {
  const mdast = { type: 'jsx', value: '<Schema src="http://foo" /> <Schema src="http://bar" />' };
  const res = pageParsing.findTypes(mdast, 'jsx', pageParsing.tagTransformer);

  expect(res.length).toBe(2);
  expect(res[0].url).toBe('http://foo');
});

describe('finds fields', () => {
  const mdast = {
    type: 'jsx', value: `<Input field="test.input" required /> <Input field="test.input2" />
<Select field="test.select" required />` };
  const res = pageParsing.findTypes(mdast, 'jsx', pageParsing.tagTransformer);

  expect(res.length).toBe(5);
  expect(res).toStrictEqual([
    { "text": ":has field", "title": "test.input", "url": "./test.input" },
    { "text": ":is required", "title": "test.input", "url": "./test.input" },
    { "text": ":has field", "title": "test.input2", "url": "./test.input2" },
    { "text": ":has field", "title": "test.select", "url": "./test.select" },
    { "text": ":is required", "title": "test.select", "url": "./test.select" }
  ]);
});

describe('finds tag and tags', () => {
  const a = { value: '<Something else="1" else2="2" />', type: markdown, children: [], depth: 0 };
  const nodes = pageParsing.getJsxTagAndTags(a);
  expect(nodes.length).toBe(1);
  expect(nodes[0].tag).toBe('Something');
  expect(nodes[0].tags).toMatchObject({ else: "1", else2: "2" });
});
