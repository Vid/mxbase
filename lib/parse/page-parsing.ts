import unified from 'unified';

import { ILink, COLLECTION, IPageMeta, TYPES, PageMap, StringMap, HAS_FIELD, USES_SCHEMA, IS_REQUIRED } from '../constants';
import { hasKlass, getRelativePathed, resolveSchemaUri, } from '../paths';

const remarkParse = require('remark-parse')
const stringify = require('remark-stringify')
const remarkMDX = require('remark-mdx');

const parseOptions = { pedantic: true };

const getLinksAndKlasses = (source: string, mdast: any) => {
  let links = findTypes(mdast, 'link', linkTransformer);
  let schemas = findTypes(mdast, 'jsx', tagTransformer); //.filter((i: any) => i !== undefined);

  links = [...links, ...schemas];

  const klasses: ILink[] = links.filter((l: ILink) => l.text === 'inKlass');
  links = links.filter((l: ILink) => l.text !== 'inKlass');
  // make links absolute
  links = links.map((l: any) => ({ ...l, url: (l.url.startsWith('/') || l.url.includes('//')) ? l.url : getRelativePathed(source, l.url) }));
  return { links, klasses };
}


const addMarkdownCollectionPage = (source: string, children: any, markdownField = 'source'): IPageMeta => {
  const processor = unified().use(remarkParse, parseOptions).use(stringify, {}).use(remarkMDX);
  const mdast = childrenAsMdast(children)
  const markdown = processor.stringify(mdast);
  const meta = getLinksAndKlasses(source, mdast);

  return { type: TYPES.markdown, origin: source, source: markdown, [markdownField]: markdown, mdast, changed: false, ...meta };
}

const output = (page: string, source: string, type: string, error?: string) => ({
  [page]: {
    source,
    error,
    links: [],
    klasses: [],
    type,
    changed: false,
    mdast: undefined
  }
});

export const getOutputs = (page: string, source: string, type: string) => {
  if (!type) {
    console.error('missing type', page);
    type = TYPES.markdown;
  }

  if (type === TYPES.markdown) {
    return getMarkdownOutputs(page, source);
  }
  return output(page, source, type);
}

type astNode = {
  type: string;
  value: string;
  children: any;
  depth: number;
};

function asMDX(source: string) {
  const processor = unified().use(remarkParse, parseOptions);
  return { mdast: processor.parse(source), jsx: {} };
}

export const getMarkdownOutputs = (page: string, source: string) => {
  const found: PageMap = {};
  let parsed;
  try {
    parsed = asMDX(source);
  } catch (error) {
    console.error('getOutputs failed for', page, { error, source });
    return output(page, source, TYPES.markdown, error);
  }
  const { mdast, jsx } = parsed;
  let { links, klasses } = getLinksAndKlasses(page, mdast);
  if (hasKlass(klasses, COLLECTION) && mdast.children) {
    const newFound = getContainerPages(mdast, page, source, klasses);
    Object.keys(newFound).forEach(k => found[k] = newFound[k]);
    links = [];
  }
  found[page] = { source, typeData: { jsx }, mdast, links, klasses, type: TYPES.markdown, changed: false /*, hast*/ };

  return found;
}

const childrenAsMdast = (children: any) => ({ type: 'root', children });

export const getJsxTagAndTags = (c: astNode) => {
  const els = c.value.split('>');
  const nodes = els.filter(a => a && a.length > 0).map(a => {
    const tag = a.trim().split(' ')[0].replace(/^</, '');
    const tags = a.split(' ').reduce<StringMap>((a, i) => ({ ...a, [i.split('=')[0]]: (i.split('=')[1] || '="true"').replace(/["']/g, '') }), {});
    return { tag, tags };
  });
  return nodes;
}

export const tagTransformer = (c: astNode) => {
  let found: ILink[] = [];
  const nodes = getJsxTagAndTags(c);

  nodes.forEach(({ tag, tags }) => {
    if (tag === 'Schema') {
      if (tags.src) {
        found.push({
          title: tags.src,
          url: resolveSchemaUri(tags.src, './'),
          text: USES_SCHEMA
        });
      }
    } else if (['Input', 'Select'].includes(tag) && tags.field) {
      found.push({
        title: tags.field,
        url: resolveSchemaUri(tags.field, './'),
        text: HAS_FIELD
      });
      if (tags.required) {
        // FIXME this should probably have the subject of the fieldSet
        found.push({
          title: tags.field,
          url: resolveSchemaUri(tags.field, './'),
          text: IS_REQUIRED
        });
      }
    }
  });
  return found;
}

const linkTransformer = (e: any) => {
  const link = {
    title: e.title,
    url: e.url,
    text: getLinkType(e.children.reduce((a: string, c: any) => c.type === 'text' ? a + c.value : a, ''))
  }
  return [link];
}

function getContainerPages(mdast: any, page: string, source: string, klasses: ILink[]) {
  const found: PageMap = {};
  let cur: string | undefined;
  let children: any[] = [];
  // process children separately so their klasses can be removed from the collection.
  const childrenPages: {
    [name: string]: {
      page: string;
      children: any;
    };
  } = {};
  mdast.children.forEach((c: astNode) => {
    // wip
    // console.log('xx', c.type, c);
    // if (c.type === 'jsx') {
    //   const nodes = getJsxTagAndTags(c);
    //   if (nodes && nodes.find(n => n.tag === 'SubPage')) {
    //     if (cur) {
    //       childrenPages[cur] = { page, children };
    //     }
    //     children = [];
    //     cur = getRelativePathed(page, c.children[0].value);
    //   }
    // }
    if (c.type === 'heading' && c.depth === 1) {
      if (cur) {
        childrenPages[cur] = { page, children };
      }
      children = [];
      cur = getRelativePathed(page, c.children[0].value);
    } else {
      if (cur) {
        children.push(c);
      }
    }
  });
  // include the remainder
  if (cur) {
    childrenPages[cur] = { page, children };
  }
  // transform the pages
  const tPages: PageMap = {};
  Object.keys(childrenPages).forEach(c => {
    const cpage = childrenPages[c];
    let mPage;
    try {
      mPage = addMarkdownCollectionPage(cpage.page, cpage.children, 'sourced');
      tPages[c] = mPage;
    }
    catch (error) {
      console.error('addMarkdownCollectionPage failed for', c, { error, children: cpage.children });
      tPages[c] = { ...output(page, source, TYPES.markdown, error), links: [], klasses: [], changed: false, mdast: undefined, type: TYPES.markdown };
    }
  });
  const childrenKlasses: ILink[] = Object.keys(childrenPages).reduce<ILink[]>((all, a) => [...all, ...tPages[a].klasses], []);
  // klasses that are in the container but not in children
  const containerKlasses = klasses.filter((k: ILink) => k.url !== COLLECTION)
    .filter((k: ILink) => !childrenKlasses.find((c: ILink) => c.text === k.text && c.url === k.url));
  // now add the container klasses and put them in found
  Object.keys(childrenPages).forEach(c => {
    const cpage = tPages[c];
    cpage.klasses = [...cpage.klasses, ...containerKlasses];
    found[c] = cpage;
  });
  return found;
}

function getLinkType(str: string) {
  const link = str.replace(/^.*:/, '')
  if (!link || link.length < 1) {
    return 'blank link';
  }
  return link;
}

/*
Update page metadata from markdown import
 */
export const pagesFromJSON = (base: string, pagesJSON: { [pageName: string]: IPageMeta }, doStore: (pageName: string, page: IPageMeta) => void) => {
  const based = (page: string) => `${base}${page}`;
  const pages = Object.keys(pagesJSON).reduce<PageMap>((all, p) => {
    const ps = getOutputs(based(p), pagesJSON[p].source!, pagesJSON[p].type);

    if (ps) {
      Object.keys(ps).forEach((ip: string) => {
        all[ip] = ps[ip];
      });
    }
    return all;
  }, {})
  Object.keys(pages).forEach((p: string) => {
    doStore(p, pages[p]);
  });

  return pages;
}


export function findTypes(mdast: any, type: string, transformer: (e: any) => any) {
  if (!mdast) {
    return [];
  }
  let found: any = [];
  const rc = (e: any) => {
    if (e.type === type) {
      found = found.concat(transformer(e));
    }
    if (e.children) {
      e.children.forEach((e: any) => {
        rc(e);
      });
    }
  }
  rc(mdast);

  return found;
}
