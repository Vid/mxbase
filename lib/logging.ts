import * as path from 'path';
const tripleBeam = require('triple-beam');
import winston from 'winston';

export const logger = winston.createLogger({
  defaultMeta: { service: 'mxbase' },
});

if (process.env.NODE_ENV === 'production') {
  logger.add(new winston.transports.File({
    filename: 'combined.log',
    level: 'info'
  }));
  logger.add(new winston.transports.File({
    filename: 'errors.log',
    level: 'error'
  }));
} else if (process.env.NODE_ENV === 'development') {
  defaultLogger(false, 'info');
} else {
  const inTest = process.env.NODE_ENV === 'test';
  defaultLogger(inTest, 'debug');
}
function defaultLogger(silent, level) {
  logger.add(new winston.transports.Console({
    silent,
    level,
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.padLevels(),
      winston.format.errors({ stack: true }),
      winston.format.simple(),
      winston.format.label({ label: path.basename((process.mainModule || { filename: 'test' }).filename) }),
      winston.format.printf(info => {
        const splat = info[tripleBeam.SPLAT];
        return (`${info.level} [${info.label}]: ${info.message}`) + (splat ? (` ` + splat.map(s => JSON.stringify(s)).join(' ')) : '');
      })
    ),
  }));
}