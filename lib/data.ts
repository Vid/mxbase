import fs from 'fs';
import path from 'path';
import fetch from 'isomorphic-fetch';
import jsonld from 'jsonld';
import { isURI } from './paths';
import { logger } from './logging';
import { assert } from './util';

export const valueFields = ['text', 'integer', 'float', 'date', 'time', 'boolean'].map(t => 'value_' + t.charAt(0).toUpperCase() + t.substr(1));
export type TDBRow = {
    fieldID: number,
    subject: string,
    predicate: string,
    isType: string,
    value_Text?: string,
    value_Integer?: number,
    value_Float?: number,
    value_Date?: number,
    value_Time?: number,
    value_Boolean?: boolean,
    apex: boolean
}

export type TJoinRow = TDBRow & { graph: string };

const DBBaseTypes = {
    'https://schema.org/Boolean': {
        suffix: 'Boolean',
        type: 'integer',
        store: (value: boolean) => value ? 1 : 0,
        get: (value: number) => value ? true : false
    },
    'https://schema.org/Text': {
        suffix: 'Text',
        type: 'text'
    },
    'https://schema.org/Date': {
        suffix: 'Date',
        type: 'integer',
        store: (value: string) => new Date(value).getTime(),
        get: (value: number) => new Date(value)
    },
    'https://schema.org/DateTime': {
        suffix: 'DateTime'
    },

    'https://schema.org/Number': {
        suffix: 'Number'
    },

    'https://schema.org/Float': {
        suffix: 'Float'
    },

    'https://schema.org/Integer': {
        suffix: 'Integer'
    },

    'https://schema.org/Time': {
        suffix: 'Time'
    },
    // FIXME bogus
    'https://www.w3.org/ns/Resource': {
        type: 'text',
        suffix: 'Text'
    },
}

const SchemaBaseTypes = Object.keys(DBBaseTypes).reduce((dbs, i) => ({ ...dbs, [i]: { db: DBBaseTypes[i] } }), {});
const remoteTypes: { [name: string]: TTypedField } = {
    'http://www.w3.org/ns/ldp#member': {
        // FIXME bogus
        type: 'https://www.w3.org/ns/Resource'
    },
    'https://schema.org/Person': {
        type: 'https://schema.org/Thing'
    },
    'https://schema.org/Thing': {
        type: 'https://schema.org/Text'
    },
    'https://schema.org/URL': {
        type: 'https://schema.org/Text'
    },
    id: {
        type: 'id'
    }
};

export class FileCachingNetworkContextRetriever {
    local: string;
    site_base: string;
    constructor(local: string, site_base: string) {
        assert({ local, site_base });
        this.local = local;
        this.site_base = site_base;
    }
    // get and expand remote schema, from cache if available
    async get(context: any) {
        const uri = typeof context === 'object' ? context['@id'] : context;
        const n = path.join(this.site_base, 'cache') + encodeURIComponent(uri);
        let schema;
        if (fs.existsSync(n) && fs.statSync(n)) {
            schema = JSON.parse(fs.readFileSync(n, 'utf-8'));
        }
        if (!schema) {
            logger.debug('not cached', uri)
            const found = await this.retrieve(uri);
            if (found) {
                schema = found;
                if (!uri.startsWith(this.local)) {
                    fs.writeFileSync(n, JSON.stringify(schema, null, 2));
                }
            }
        }
        if (schema) {
            return schema;
        } else {
            throw Error(`could not retrieve ${uri}`);
        }
    }
    async retrieve(uri) {
        const response = await fetch(uri, { headers: { Accept: 'application/ld+json' } });
        if (response.ok) {
            return await response.json();
        }
        return null;
    }
}

interface IRetriever {
    get: (uri: any) => any;
}

type TTypedField = {
    db?: {
        suffix: string,
        type: string
    },
    type?: string,
    uri?: string
}


function withDomain(domain, path) {
    return isURI(path) ? path : `${domain}/${path}`;
}
// returns a row for the ID
export const getIdRow = (subject) => ({ fieldID: 0, subject, predicate: 'id', value_Text: subject, isType: 'id', apex: true });

export class DataProcessor {
    host: string;
    retriever: IRetriever;

    constructor(host: string, retriever: FileCachingNetworkContextRetriever) {
        this.host = host;
        this.retriever = retriever;
    }

    // receive data in a schema. determine its data types, and create rows to insert
    async expandToRows(schemaData): Promise<TDBRow[]> {
        const subject = schemaData.id || schemaData['@id'];
        // a map of all the types
        const types = await this.getTypes(schemaData);
        // fields mapped to types
        const fields: { [name: string]: TTypedField } = Object.keys(schemaData).filter(k => k !== '@context')
            .filter(f => f !== 'id')
            .reduce((all, f) => {
                let type;
                let uri;
                if (f === 'id') {
                    type = 'id';
                    uri = f;
                } else {
                    const field = f.includes(':') ? f : Object.keys(types).find(k => k.endsWith(`/${f}`));
                    if (!types[field]) {
                        throw Error(`no type for ${f} from ${JSON.stringify(types, null, 2)}`);
                    }
                    type = types[field].type;
                    uri = field;
                }
                if (!type) {
                    throw Error(`unknown type for ${f}`);
                }
                return { ...all, [f]: { type, uri } };
            }, {});

        // now create the database rows based on types
        // transitional id for debugging
        let fieldID = 1;
        let rows: TDBRow[] = subject ? [getIdRow(subject)] : [];
        for (let [name, field] of Object.entries(fields)) {
            fieldID++;
            const frows: TDBRow[] = [{ fieldID, subject, predicate: field.uri, isType: field.type, apex: true }];
            const value = schemaData[name];
            let cur = { ...field };
            // find interim types and base type
            while (!cur.db) {
                const next = types[cur.type];
                if (!next) {
                    throw Error(`no db or type for ${JSON.stringify(cur, null, 2)}`);
                }
                frows.push({ fieldID, subject, predicate: cur.type || cur.db.type, isType: next.type, apex: false })
                cur = next;
            }
            frows.push({ fieldID, subject, predicate: cur.type || cur.db.type, isType: cur.type, apex: false })
            frows.forEach(r => r[`value_${cur.db.suffix}`] = value);
            rows = rows.concat(frows);
        }

        return rows;
    }
    // get all the types in context for the schemaData
    async getTypes(schemaData: any) {
        let contexts = await getContexts(this.host, schemaData['@context']);
        if (!contexts) {
            throw Error('no @context');
        }
        const types = await Object.keys(contexts).reduce(async (all, c) => {
            if (!all[c]) {
                const remote = await this.retriever.get(c);
                if (!remote || !remote['@context']) {
                    throw Error(`no context ${c} ${JSON.stringify(remote, null, 2)}`);
                }
                const remoteContexts = await getContexts(c, remote['@context']);
                for (let [k, v] of Object.entries(remoteContexts)) {
                    all[k] = { ...all[k], ...v };
                }
            }
            return all;
        }, { ...SchemaBaseTypes, ...remoteTypes });
        return types;
    }
}

// take results from a query, remove fields that aren't defined
export const defined = (rows) => {
    let valueRows = rows.map(row => {
        let valueRow = Object.keys(row).reduce((all, r) => {
            if (row[r] !== null) {
                if (valueFields.includes(r)) {
                    return { ...all, [r]: row[r], value: row[r], type: row.isType };
                }
                return { ...all, [r]: row[r] };
            }
            return all;
        }, {});
        return valueRow;
    });
    return valueRows;
}
// takes apex db rows and expands them to a form compatible with jsonld-js
export function expandRows(rows) {
    const d = defined(rows).filter(d => d.isType !== 'id');
    const expanded = d.reduce((all, row: TDBRow) => ({ ...all, [row.isType]: [{ '@value': (row as any).value }] }), {});
    return [expanded];
}
// receive rows from a data query, re-assemble it to a schema
export async function compactRows(rows: TJoinRow[]) {
    const context = getContextFromJoinRows(rows);
    const expanded = expandRows(rows);
    try {
        const compacted = await jsonld.compact(expanded, context);
        return compacted;
    } catch (e) {
        console.error(e);
        throw (e);
    }
}

// FIXME guesses the context
function getContextFromJoinRows(rows) {
    const s = rows.find(r => r.predicate.includes('/'));
    return [s.predicate.substr(0, s.predicate.lastIndexOf('/'))];
}

export const getIso8601Date = (value = Date.now()) => new Date(value).toISOString().split('.')[0] + 'Z';

// decode the @context section to an array of URIs
// handle [contexts], {context: uri}, or {context:{@id: uri}}
async function getContexts(source, docContext): Promise<{ [uri: string]: { uri: string } }> {
    if (typeof docContext === 'string') {
        return { [docContext]: { uri: docContext } };
    } else if (Array.isArray(docContext)) {
        return docContext.reduce((all, c) => {
            if (typeof c === 'string') {
                return { ...all, [c]: { uri: c } };
            } else if (c['@id']) {
                return { ...all, [c['@id']]: { ...c, uri: c } };
            } else {
                throw Error(`unknown context type ${JSON.stringify({ context: c, docContext, source }, null, 2)}`);
            }
        }, {});
    } else if (Object.keys(docContext).length > 0) {
        return Object.entries(docContext).reduce((all, [k, v]) => {
            const key = k.includes(':') ? k : withDomain(source, k);
            if (v['@id']) {
                return { ...all, [key]: { type: v['@id'], ...<Object>v } };
            }
            return { ...all, [key]: { type: v } };
        }, {});
    } else {
        throw Error(`no array ${JSON.stringify(docContext, null, 2)}`);
    }
}


export function byGraph(exp) {
    return exp.reduce((a, b) => {
        return a[b.graph] ? { ...a, [b.graph]: [...a[b.graph], b] } : { ...a, [b.graph]: [b] }
    }, {});
}
