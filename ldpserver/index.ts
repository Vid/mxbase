import fs from 'fs';
import mkdirp from 'mkdirp';

import { safePath } from '../lib/util';
import { TExtension } from '../mxbase';
import { logger } from '../lib/logging';
import { getHostPageId, schemaInNS, a } from '../lib/paths';
import { containerSchemaName } from '../lib/constants';
import { DataProcessor, FileCachingNetworkContextRetriever } from '../lib/data';

class VersionedFS {
    site_base: string;
    MARKER = '.__';
    constructor(site_base: string) {
        this.site_base = site_base;
    }
    private getLocationAndContent(pathIn: string) {
        const location = safePath(this.site_base, `/a/${pathIn}${this.MARKER}`);
        const content = `${location}/CONTENT`
        return { location, content };
    }
    async send(pathIn: string, res) {
        if (pathIn.endsWith(this.MARKER)) {
            return { error: `cannot end with ${this.MARKER}` };
        }
        const { content } = this.getLocationAndContent(pathIn);
        if (fs.existsSync(content)) {
            return res.sendFile(content);
        }
        res.status(404).text(`File not found`);
    }
    async write(pathIn: string, contents: string): Promise<{ status: number, message?: string, updated?: boolean }> {
        if (pathIn.endsWith(this.MARKER)) {
            return { status: 500, message: `cannot end with ${this.MARKER}` };
        }
        try {
            const { location, content } = this.getLocationAndContent(pathIn);
            if (!fs.existsSync(location)) {
                mkdirp.sync(location);
            }
            if (fs.existsSync(content)) {
                fs.renameSync(content, `${content}.${Date.now()}`);
                fs.writeFileSync(content, contents, 'utf-8');
                return { status: 201, updated: true };
            } else {
                fs.writeFileSync(content, contents, 'utf-8');
                return { status: 201, updated: false };
            }
        } catch (e) {
            throw Error(`can't save file ${e.message}`);
        }
    }
}

export const ldp: TExtension = async ({ app, checkAuth, getConfig, db, defaultRoute }) => {
    const checker = checkAuth.checker();
    const config = await getConfig();
    const dp = new DataProcessor(config.host, new FileCachingNetworkContextRetriever(config.host, config.site_base));
    const vfs = new VersionedFS(config.site_base);

    // FIXME page requests handle fe and individual requests
    app.get('/a/*', async (req, res) => {
        const contentType = req.headers['content-type'];
        if (contentType === 'text/html') {
            defaultRoute(req, res);
        }
        const pathIn = req.originalUrl.replace(/.*?\/a\//, '');
        await vfs.send(pathIn, res);
    });
    app.post('/a/:path*', checker, async (req, res) => {
        const { contents } = req.body;
        const pathIn = req.originalUrl.replace(/.*?\/a\//, '');
        const { status, updated, message } = await vfs.write(pathIn, contents);
        if (status !== 201) {
            res.status(status).text(message);
            return;
        }
        try {
            const username = res.locals.auth.username;
            const { jsonld, schema, graph } = getContainer(await getConfig(), pathIn);
            await db.saveExpandedRows(dp.expandToRows(jsonld), { username, schema, graph });
            res.status(status).set('Location', a(pathIn)).set('Updated', updated).json({ updated });
        } catch (e) {
            logger.error('saving data', e.message);
            res.status(500).end(e.message);
        }
    });
}

function getContainer(config, pathIn: string) {
    const schema = containerSchemaName;
    const member = pathIn.replace(/\/.*?$/, '');
    const graph = getHostPageId(config.host, pathIn)
    const jsonld = {
        '@context': [schemaInNS(config.host, containerSchemaName)],
        member
    }
    return { jsonld, schema, graph };
}