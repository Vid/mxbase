import * as failFast from 'jasmine-fail-fast'

if (JSON.parse(process.env.npm_config_argv).original.includes('--bail')) {
    const jasmineEnv = (jasmine as any).getEnv();
    jasmineEnv.addReporter(failFast.init());
}