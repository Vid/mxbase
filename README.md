
# mx (temporary working name)

[![pipeline status](https://gitlab.com/Vid/mxbase/badges/master/pipeline.svg)](https://gitlab.com/Vid/mxbase/-/commits/master)
[![NPM Version](https://img.shields.io/npm/v/mxbase.svg?style=flat-square)](https://npm.im/vc-js)

Designed to be a base for open-ended Express/Solid apps while the world figures out these details properly.

** WIP WIP WIP WIP **

Each site instance is separate from mxbase, so multiple sites can be managed with minimal customization per site.

It has a rudimentary express route to sign verifiable credentials using vc-js, plus some other stub features.

In this folder:

`npm install`

Then create your instance, create an issue if you want to do that.

For instructions for setting up alongside Solid, see [instructions](setup.md).

# Test notes

use `NODE_ENV development` to see everything but debug logging.

Can't get bail to work via package.json. use this:

`npm run test-watch-logging -- --bail`

also, --notify --silent --verbose
