
import { writeKeys, sign } from '../vc/';
import { getHostDataId } from '../lib/paths';

export const keys = ({ app, checkAuth, getConfig, logger, db }) => {
    const checker = checkAuth.checker();
    app.get('/app/account/newKeys', checker, async (req, res) => {
        const auth = res.locals.auth;
        const config = await getConfig();
        writeKeys(config.host, config.site_base, auth.username);
        res.json({ result: 'OK' });
    });
    app.post('/app/sign', checker, async function (req, res) {
        const config = await getConfig();
        const { username } = res.locals.auth;
        const { schema, subject } = req.body;
        delete subject['@context'];

        let signed;
        let vcId;
        try {
            vcId = (await db.getID({ username, schema: 'verifiableCredential/v1' })).id;
            const credentialID = getHostDataId(config.host, 'verifiableCredential/v1', vcId);
            signed = await sign({ credentialID, issuer: username, subject, schema, config });
        } catch (e) {
            logger.warn('error signing', e);
            return res.status(500).json({ error: e });
        }
        let result;
        try {
            result = await db.saveData({ username, schema: 'verifiableCredential/v1', data: signed, host: config.host, id: vcId });
            res.json({ signed });
        } catch (e) {
            logger.error('error saving signed', e);
            return res.status(500).json({ error: e, result });
        }
    });


}