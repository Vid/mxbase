
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcryptjs';
import { writeKeys } from '../vc/';

export const accounts = ({ app, checkAuth, getConfig, logger, db }) => {
    const checker = checkAuth.checker();
    app.post('/app/account/register', async (req, res) => {
        const config = await getConfig();
        const { username, emailAddress, password } = req.body;
        if (!username || !password || !emailAddress) {
            return res.status(500).send('Missing field');
        }

        const result = await db.createUser({ username, emailAddress, password });
        logger.info('createUser', username);
        /*
        if (err.errno === 19) {
          const [, , , , field] = err.message.split(' ');
          return res.status(409).send(field);
        }
        return res.status(500).send(`error ${err.errno}`);
        */
        const user = await db.findUserByUsername(username);
        // if (err) return res.status(500).send('findUser error!');
        writeKeys(config.host, config.site_base, username);
        sendAccessToken(user.id, username, res);
    });

    app.post('/app/account/login', async (req, res) => {
        const { username, password } = req.body;
        if (!username || !password) {
            return res.status(500).send('Missing field');
        }
        const user = await db.findUserByUsername(username);
        // logger.warn('login error', err);
        // return res.status(500).json({ json: err.errno });
        if (!user) {
            return res.status(404).json('No such user');
        }
        const result = bcrypt.compareSync(password, user.password);
        if (!result) {
            return res.status(401).send('Password not valid!');
        }
        sendAccessToken(user.id, username, res);
    });

    app.get('/app/account/logout', (req, res) => {
        res.status(200).send({ username: undefined, access_token: undefined });
    });

    const sendAccessToken = (id, username, res) => {
        const expiresIn = 24 * 60 * 60;
        const access_token = jwt.sign({ id, username }, checkAuth.SECRET_KEY, { expiresIn });
        res.status(200).send({ username, access_token, expires_in: expiresIn });
    }

    app.post('/app/account/delete', checker, async (req, res) => {
        const auth = res.locals.auth;
        const { username, password } = req.body;
        if (!username || !password || username !== auth.username) {
            return res.status(404).send('Missing field');
        }

        const user = await db.findUserByUsername(username);
        // if (err) {
        //   logger.warn('account delete error', err);
        //   return res.status(500).json({ json: err.errno });
        // }
        if (!user) {
            return res.status(404).json('No such user');
        }
        const result = bcrypt.compareSync(password, user.password);
        if (!result) {
            return res.status(401).send('Password not valid!');
        }
        await db.deleteByUsername(username);
        // if (err) {
        //   return res.status(500).json({ json: err });
        // }
        logger.info('deleted user', username);
        res.status(200).send({ username, access_token: {}, expires_in: 0 });
    });

}

export default accounts;

export class CheckAuth {
    SECRET_KEY: string;
    logger: any;
    constructor(key: string, logger) {
        this.SECRET_KEY = key;
        this.logger = logger;
    }
    checker() {
        return this.check.bind(this);
    }
    check(req, res, next) {
        const header = req.headers['authorization'];

        if (typeof header !== 'undefined') {
            const token = header.split(' ')[1];
            try {
                const auth = jwt.verify(token, this.SECRET_KEY);
                res.locals.auth = auth;
                next();
            } catch (e) {
                this.logger.warn('jwt exception', JSON.stringify(e));
                res.setHeader('WWW-Authenticate', 'Bearer token_type="JWT"');
                res.status(401).send('Invalid token');
            }
        } else {
            res.sendStatus(401);
            this.logger.warn('unauthorized', req.originalUrl);
        }
    }
}
