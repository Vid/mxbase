import { APP_NS } from '../lib/constants';
import { getSchemaVer } from '../lib/paths';

export const schema = ({ app, checkAuth, getConfig, logger, db }) => {
    const checker = checkAuth.checker();

    app.get(`${APP_NS}/:schemaName/:version`, async (req, res) => {
        const { schemaName, version } = req.params;
        const schema = getSchemaVer(schemaName, version);
        const config = await getConfig();
        if (config.schemas && config.schemas[schema]) {
            res.contentType('application/ld+json').
                send(JSON.stringify(config.schemas[schema].schema));
        } else {
            res.status(404).send('Schema not found');
        }
    });
    app.get(APP_NS, async (req, res) => {
        const config = await getConfig();
        res.json(Object.keys(config.schemas).map(s => s));
    });

    app.delete('/app/data/:schemaName/:version/:id', checker, async function (req, res) {
        const { schemaName, version, id } = req.params;
        const schema = getSchemaVer(schemaName, version);
        const { username } = res.locals.auth;
        await db.deleteData({ schema, id, username });
        logger.info('deleteData', schema, username);
        res.status(200).end({ result: 'deleted' })
    });

    app.post('/app/data/:schemaName/:version/:id?', checker, async function (req, res) {
        const config = await getConfig();
        const { schemaName, version, id } = req.params;
        const schema = getSchemaVer(schemaName, version);
        const { data } = req.body;
        const { username } = res.locals.auth;

        const result = await db.saveData({ schema, id: parseInt(id), username, data, host: config.host });
        logger.info('saveData', id && 'withID', schema, id, data, result);
        res.status(201).json({ result })
    });

    app.get('/app/data/:schemaName/:version/:id?', checker, async function (req, res) {
        const { schemaName, version, id } = req.params;
        const schema = getSchemaVer(schemaName, version);
        const { username } = res.locals.auth;
        const rows = await db.findData({ schema, id, username });
        logger.info('findData', schemaName, version, id, rows.length, rows.length < 1);
        res.json({ rows: (rows as any[]).map(r => ({ ...r, data: JSON.parse(r.data) })) });
    });


}