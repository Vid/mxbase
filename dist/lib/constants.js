"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.idField = 'id';
exports.APP_NS = '/app/ns';
exports.TYPES = {
    folder: 'folder',
    text: 'text/plain',
    markdown: 'text/markdown',
    turtle: 'text/turtle',
    jsonld: 'application/ld+json'
};
exports.COLLECTION = 'Collection';
exports.IS_REQUIRED = ':is required';
exports.USES_SCHEMA = ':uses schema';
exports.HAS_FIELD = ':has field';
exports.getContentType = (type) => type === 'json' ? 'application/json' : 'text/plain';
exports.containerSchemaName = 'container/v1';
exports.containerDef = {
    [exports.containerSchemaName]: {
        schema: {
            "@context": {
                "member": {
                    "@id": "http://www.w3.org/ns/ldp#member",
                    "@type": "@id"
                },
                "title": "http://purl.org/dc/terms/title",
                "dcterms": "http://purl.org/dc/terms/",
                "ldp": "http://www.w3.org/ns/ldp#",
                "rdfs": "http://www.w3.org/2000/01/rdf-schema#"
            }
        }
    }
};
//# sourceMappingURL=constants.js.map