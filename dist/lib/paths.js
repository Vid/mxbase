"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("./constants");
exports.schemaInNS = (host, schema) => `${host}/app/ns/${schema}`;
exports.issuerKeyBase = (host) => `${host}/app/key/`;
exports.issuerKeyLoc = (host, username) => `${host}/app/key/${username}.json`;
exports.withSchema = ({ data, id, schema, host }) => JSON.stringify({ '@context': [exports.schemaInNS(host, schema)], ...data, [constants_1.idField]: exports.getHostDataId(host, schema, id) });
exports.getHostDataId = (host, schema, id) => host + `/app/data/${schema}/` + (id !== undefined ? id : '');
exports.a = (where) => exports.isURI(where) ? where : ('/a/' + exports.getFolder(where.replace(/^\//, ''))) + encodeURIComponent(exports.getFile(where));
exports.getHostPageId = (host, where) => exports.isURI(where) ? where : (`${host}/a/` + exports.getFolder(where.replace(/^\//, ''))) + encodeURIComponent(exports.getFile(where));
exports.hasKlassIntersection = (klasses, cont) => {
    return !!klasses.find((k) => cont.find((c) => k.url === c.url));
};
exports.hasKlass = (klasses, klass) => {
    return !!klasses.map(k => k.url).includes(klass);
};
exports.isVirtual = (page) => page.includes('/(');
exports.isURI = (what) => what && what.includes('://');
// FIXME following shouldn't be here
exports.getRelativePathed = (ground, path) => {
    if (path.startsWith('/')) {
        return path;
    }
    const folder = exports.getFolder(ground);
    const file = (exports.getFile(path) || '').replace(/ /g, '_');
    return `${folder}${file}`;
};
exports.getFolder = (location) => {
    if (!location) {
        console.error('no location');
        return '';
    }
    return location.substr(0, location.lastIndexOf('/') + 1);
};
// make a file relative to ground
exports.getFile = (location) => {
    const res = location.substr(location.lastIndexOf('/') + 1);
    return res.length ? res : undefined;
};
exports.resolveSchemaUri = (src, ground) => exports.isURI(src) ? src : exports.getRelativePathed(ground, src);
function getSchemaVer(schema, version) {
    if (!schema || !version) {
        throw Error(`missing schema component "${schema}" "${version}"`);
    }
    return `${schema}/${version}`;
}
exports.getSchemaVer = getSchemaVer;
//# sourceMappingURL=paths.js.map