"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const isomorphic_fetch_1 = __importDefault(require("isomorphic-fetch"));
const jsonld_1 = __importDefault(require("jsonld"));
const paths_1 = require("./paths");
const logging_1 = require("./logging");
const util_1 = require("./util");
exports.valueFields = ['text', 'integer', 'float', 'date', 'time', 'boolean'].map(t => 'value_' + t.charAt(0).toUpperCase() + t.substr(1));
const DBBaseTypes = {
    'https://schema.org/Boolean': {
        suffix: 'Boolean',
        type: 'integer',
        store: (value) => value ? 1 : 0,
        get: (value) => value ? true : false
    },
    'https://schema.org/Text': {
        suffix: 'Text',
        type: 'text'
    },
    'https://schema.org/Date': {
        suffix: 'Date',
        type: 'integer',
        store: (value) => new Date(value).getTime(),
        get: (value) => new Date(value)
    },
    'https://schema.org/DateTime': {
        suffix: 'DateTime'
    },
    'https://schema.org/Number': {
        suffix: 'Number'
    },
    'https://schema.org/Float': {
        suffix: 'Float'
    },
    'https://schema.org/Integer': {
        suffix: 'Integer'
    },
    'https://schema.org/Time': {
        suffix: 'Time'
    },
    // FIXME bogus
    'https://www.w3.org/ns/Resource': {
        type: 'text',
        suffix: 'Text'
    },
};
const SchemaBaseTypes = Object.keys(DBBaseTypes).reduce((dbs, i) => ({ ...dbs, [i]: { db: DBBaseTypes[i] } }), {});
const remoteTypes = {
    'http://www.w3.org/ns/ldp#member': {
        // FIXME bogus
        type: 'https://www.w3.org/ns/Resource'
    },
    'https://schema.org/Person': {
        type: 'https://schema.org/Thing'
    },
    'https://schema.org/Thing': {
        type: 'https://schema.org/Text'
    },
    'https://schema.org/URL': {
        type: 'https://schema.org/Text'
    },
    id: {
        type: 'id'
    }
};
class FileCachingNetworkContextRetriever {
    constructor(local, site_base) {
        util_1.assert({ local, site_base });
        this.local = local;
        this.site_base = site_base;
    }
    // get and expand remote schema, from cache if available
    async get(context) {
        const uri = typeof context === 'object' ? context['@id'] : context;
        const n = path_1.default.join(this.site_base, 'cache') + encodeURIComponent(uri);
        let schema;
        if (fs_1.default.existsSync(n) && fs_1.default.statSync(n)) {
            schema = JSON.parse(fs_1.default.readFileSync(n, 'utf-8'));
        }
        if (!schema) {
            logging_1.logger.debug('not cached', uri);
            const found = await this.retrieve(uri);
            if (found) {
                schema = found;
                if (!uri.startsWith(this.local)) {
                    fs_1.default.writeFileSync(n, JSON.stringify(schema, null, 2));
                }
            }
        }
        if (schema) {
            return schema;
        }
        else {
            throw Error(`could not retrieve ${uri}`);
        }
    }
    async retrieve(uri) {
        const response = await isomorphic_fetch_1.default(uri, { headers: { Accept: 'application/ld+json' } });
        if (response.ok) {
            return await response.json();
        }
        return null;
    }
}
exports.FileCachingNetworkContextRetriever = FileCachingNetworkContextRetriever;
function withDomain(domain, path) {
    return paths_1.isURI(path) ? path : `${domain}/${path}`;
}
// returns a row for the ID
exports.getIdRow = (subject) => ({ fieldID: 0, subject, predicate: 'id', value_Text: subject, isType: 'id', apex: true });
class DataProcessor {
    constructor(host, retriever) {
        this.host = host;
        this.retriever = retriever;
    }
    // receive data in a schema. determine its data types, and create rows to insert
    async expandToRows(schemaData) {
        const subject = schemaData.id || schemaData['@id'];
        // a map of all the types
        const types = await this.getTypes(schemaData);
        // fields mapped to types
        const fields = Object.keys(schemaData).filter(k => k !== '@context')
            .filter(f => f !== 'id')
            .reduce((all, f) => {
            let type;
            let uri;
            if (f === 'id') {
                type = 'id';
                uri = f;
            }
            else {
                const field = f.includes(':') ? f : Object.keys(types).find(k => k.endsWith(`/${f}`));
                if (!types[field]) {
                    throw Error(`no type for ${f} from ${JSON.stringify(types, null, 2)}`);
                }
                type = types[field].type;
                uri = field;
            }
            if (!type) {
                throw Error(`unknown type for ${f}`);
            }
            return { ...all, [f]: { type, uri } };
        }, {});
        // now create the database rows based on types
        // transitional id for debugging
        let fieldID = 1;
        let rows = subject ? [exports.getIdRow(subject)] : [];
        for (let [name, field] of Object.entries(fields)) {
            fieldID++;
            const frows = [{ fieldID, subject, predicate: field.uri, isType: field.type, apex: true }];
            const value = schemaData[name];
            let cur = { ...field };
            // find interim types and base type
            while (!cur.db) {
                const next = types[cur.type];
                if (!next) {
                    throw Error(`no db or type for ${JSON.stringify(cur, null, 2)}`);
                }
                frows.push({ fieldID, subject, predicate: cur.type || cur.db.type, isType: next.type, apex: false });
                cur = next;
            }
            frows.push({ fieldID, subject, predicate: cur.type || cur.db.type, isType: cur.type, apex: false });
            frows.forEach(r => r[`value_${cur.db.suffix}`] = value);
            rows = rows.concat(frows);
        }
        return rows;
    }
    // get all the types in context for the schemaData
    async getTypes(schemaData) {
        let contexts = await getContexts(this.host, schemaData['@context']);
        if (!contexts) {
            throw Error('no @context');
        }
        const types = await Object.keys(contexts).reduce(async (all, c) => {
            if (!all[c]) {
                const remote = await this.retriever.get(c);
                if (!remote || !remote['@context']) {
                    throw Error(`no context ${c} ${JSON.stringify(remote, null, 2)}`);
                }
                const remoteContexts = await getContexts(c, remote['@context']);
                for (let [k, v] of Object.entries(remoteContexts)) {
                    all[k] = { ...all[k], ...v };
                }
            }
            return all;
        }, { ...SchemaBaseTypes, ...remoteTypes });
        return types;
    }
}
exports.DataProcessor = DataProcessor;
// take results from a query, remove fields that aren't defined
exports.defined = (rows) => {
    let valueRows = rows.map(row => {
        let valueRow = Object.keys(row).reduce((all, r) => {
            if (row[r] !== null) {
                if (exports.valueFields.includes(r)) {
                    return { ...all, [r]: row[r], value: row[r], type: row.isType };
                }
                return { ...all, [r]: row[r] };
            }
            return all;
        }, {});
        return valueRow;
    });
    return valueRows;
};
// takes apex db rows and expands them to a form compatible with jsonld-js
function expandRows(rows) {
    const d = exports.defined(rows).filter(d => d.isType !== 'id');
    const expanded = d.reduce((all, row) => ({ ...all, [row.isType]: [{ '@value': row.value }] }), {});
    return [expanded];
}
exports.expandRows = expandRows;
// receive rows from a data query, re-assemble it to a schema
async function compactRows(rows) {
    const context = getContextFromJoinRows(rows);
    const expanded = expandRows(rows);
    try {
        const compacted = await jsonld_1.default.compact(expanded, context);
        return compacted;
    }
    catch (e) {
        console.error(e);
        throw (e);
    }
}
exports.compactRows = compactRows;
// FIXME guesses the context
function getContextFromJoinRows(rows) {
    const s = rows.find(r => r.predicate.includes('/'));
    return [s.predicate.substr(0, s.predicate.lastIndexOf('/'))];
}
exports.getIso8601Date = (value = Date.now()) => new Date(value).toISOString().split('.')[0] + 'Z';
// decode the @context section to an array of URIs
// handle [contexts], {context: uri}, or {context:{@id: uri}}
async function getContexts(source, docContext) {
    if (typeof docContext === 'string') {
        return { [docContext]: { uri: docContext } };
    }
    else if (Array.isArray(docContext)) {
        return docContext.reduce((all, c) => {
            if (typeof c === 'string') {
                return { ...all, [c]: { uri: c } };
            }
            else if (c['@id']) {
                return { ...all, [c['@id']]: { ...c, uri: c } };
            }
            else {
                throw Error(`unknown context type ${JSON.stringify({ context: c, docContext, source }, null, 2)}`);
            }
        }, {});
    }
    else if (Object.keys(docContext).length > 0) {
        return Object.entries(docContext).reduce((all, [k, v]) => {
            const key = k.includes(':') ? k : withDomain(source, k);
            if (v['@id']) {
                return { ...all, [key]: { type: v['@id'], ...v } };
            }
            return { ...all, [key]: { type: v } };
        }, {});
    }
    else {
        throw Error(`no array ${JSON.stringify(docContext, null, 2)}`);
    }
}
function byGraph(exp) {
    return exp.reduce((a, b) => {
        return a[b.graph] ? { ...a, [b.graph]: [...a[b.graph], b] } : { ...a, [b.graph]: [b] };
    }, {});
}
exports.byGraph = byGraph;
//# sourceMappingURL=data.js.map