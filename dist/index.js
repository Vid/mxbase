"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants = __importStar(require("./lib/constants"));
exports.constants = constants;
const paths = __importStar(require("./lib/paths"));
exports.paths = paths;
const pageParsing = __importStar(require("./lib/parse/page-parsing"));
exports.pageParsing = pageParsing;
var mxbase_int_1 = require("./test/int/mxbase.int");
exports.tests = mxbase_int_1.tests;
var vc_1 = require("./vc/");
exports.verifyCredential = vc_1.verifyCredential;
//# sourceMappingURL=index.js.map