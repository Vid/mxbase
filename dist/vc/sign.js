"use strict";
/*
 * Based on a sample by Digital Bazaar.
*/
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = __importStar(require("fs"));
const vc_js_1 = __importDefault(require("vc-js"));
const { Ed25519KeyPair, suites: { Ed25519Signature2018 } } = require('jsonld-signatures');
const data_1 = require("../lib/data");
const _1 = require("./");
const paths_1 = require("../lib/paths");
// retrieve the signing elements and return the signed document
exports.sign = async ({ credentialID, issuer, subject, schema, config }) => {
    const publicKeyLoc = `${config.site_base}/keys/${issuer}.json`;
    const signingKeyLoc = `${config.site_base}/private/signing/${issuer}.json`;
    const publicKey = JSON.parse(fs.readFileSync(publicKeyLoc, 'utf-8'));
    const signingKey = JSON.parse(fs.readFileSync(signingKeyLoc, 'utf-8'));
    const { privateKeyBase58 } = signingKey;
    const signed = await exports.issue({ credentialID, issuerKeys: { ...publicKey, ...signingKey }, publicKey, keyID: paths_1.issuerKeyLoc(config.host, issuer), subject, schema, privateKeyBase58, config });
    return signed;
};
// issue a credential with the passed elements
exports.issue = async ({ credentialID, issuerKeys, publicKey, keyID, subject, schema, privateKeyBase58, config }) => {
    publicKey.privateKeyBase58 = privateKeyBase58;
    const keyPair = new Ed25519KeyPair(issuerKeys);
    keyPair.id = keyID;
    const suite = new Ed25519Signature2018({
        verificationMethod: keyID,
        key: keyPair
    });
    const documentLoader = _1.getDocumentLoader(config);
    delete subject['@context'];
    //  https://www.w3.org/TR/vc-data-model/#example-1-a-simple-example-of-a-verifiable-credential
    const credential = {
        '@context': [
            'https://www.w3.org/2018/credentials/v1',
            paths_1.schemaInNS(config.host, schema)
        ],
        id: credentialID,
        type: ['VerifiableCredential', schema.replace(/\/.*/, '')],
        issuer: keyPair.controller,
        issuanceDate: data_1.getIso8601Date(),
        credentialSubject: subject
    };
    try {
        const result = await vc_js_1.default.issue({
            credential,
            suite,
            documentLoader
        });
        return result;
    }
    catch (e) {
        throw (e);
    }
};
//# sourceMappingURL=sign.js.map