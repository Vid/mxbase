"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Based on a sample by Digital Bazaar.
*/
const fs = __importStar(require("fs"));
const path_1 = require("path");
class FileWriter {
    constructor(base, filename) {
        this.filename = filename;
        this.base = base;
        this.fullRawUrl = this.base + filename;
    }
    get rawUrl() {
        return this.fullRawUrl;
    }
    async create({ path, content }) {
        const out = path_1.join(path, this.filename);
        fs.writeFileSync(out, JSON.stringify(content, null, 2), null);
    }
    async update({ path, content }) {
        return this.create({ path, content });
    }
}
exports.FileWriter = FileWriter;
//# sourceMappingURL=FileWriter.js.map