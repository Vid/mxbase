"use strict";
/*
 * adapted from code by Digital Bazaar
*/
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const credentials_context_1 = __importDefault(require("credentials-context"));
const isomorphic_fetch_1 = __importDefault(require("isomorphic-fetch"));
const vc_examples_v1_1 = require("./contexts/vc-examples-v1");
const odrl_1 = require("./contexts/odrl");
const schema_1 = require("./contexts/schema");
const paths_1 = require("../lib/paths");
exports.getSchemas = (config) => {
    const { schemas } = config;
    const found = Object.keys(schemas).map(s => ([paths_1.schemaInNS(config.host, s), schemas[s].schema]));
    return found;
};
const getContext = (config) => {
    const schemas = exports.getSchemas(config);
    return new Map([
        ...credentials_context_1.default.contexts,
        ...schemas,
        [
            'https://www.w3.org/2018/credentials/examples/v1',
            vc_examples_v1_1.vcExamplesV1
        ],
        [
            'https://www.w3.org/ns/odrl.jsonld',
            odrl_1.odrl
        ],
        [
            'http://schema.org',
            schema_1.schema
        ]
    ]);
};
exports.getDocumentLoader = (config) => {
    return async (url) => {
        let document;
        const contexts = getContext(config);
        document = contexts.get(url);
        if (document) {
            return {
                contextUrl: null,
                documentUrl: url,
                document
            };
        }
        const response = await isomorphic_fetch_1.default(url);
        if (!response.ok) {
            throw Error(`cannot retrieve context ${url}`);
        }
        document = await response.json();
        return {
            contextUrl: null,
            documentUrl: url,
            document,
        };
    };
};
//# sourceMappingURL=document-loader.js.map