"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Based on a sample by Digital Bazaar.
*/
const fs_1 = __importDefault(require("fs"));
const jsonld_signatures_1 = require("jsonld-signatures");
const FileWriter_1 = require("./FileWriter");
const util_1 = require("../lib/util");
// import Secp256k1KeyPair from 'secp256k1-key-pair';
async function writeKeys(host, site_base, username) {
    util_1.mkpaths(site_base);
    // FIXME key owner is undefined
    const result = await exports.generateKey(site_base, username, host + '/app/key/', 'ed25519');
    const { privateKeyBase58, controller } = result;
    fs_1.default.writeFileSync(`${site_base}/private/signing/${username}.json`, JSON.stringify({ privateKeyBase58, controller }, null, 2));
}
exports.writeKeys = writeKeys;
exports.generateKey = async (site_base, username, base, keyType) => {
    let k;
    switch (keyType) {
        case 'ed25519':
            k = await jsonld_signatures_1.Ed25519KeyPair.generate();
            break;
        // case 'secp256k1':
        //   k = await Secp256k1KeyPair.generate();
        //   break;
        default:
            throw new Error(`Unknown key type: "${keyType}".`);
    }
    const controllerWriter = new FileWriter_1.FileWriter(base, `controller.${username}.json`);
    const keyWriter = new FileWriter_1.FileWriter(base, `${username}.json`);
    const publicKeyDoc = Object.assign(k.publicNode(), {
        '@context': 'https://w3id.org/security/v2',
        id: keyWriter.rawUrl,
        controller: controllerWriter.rawUrl
    });
    const controllerDoc = {
        '@context': 'https://w3id.org/security/v2',
        id: controllerWriter.rawUrl,
        assertionMethod: [keyWriter.rawUrl]
    };
    await Promise.all([
        controllerWriter.create({ path: `${site_base}/keys`, content: controllerDoc }),
        keyWriter.create({ path: `${site_base}/keys`, content: publicKeyDoc }),
    ]);
    return Object.assign(k, {
        id: keyWriter.rawUrl,
        controller: controllerWriter.rawUrl,
    });
};
//# sourceMappingURL=key.js.map