"use strict";
/*
 * Based on a sample by Digital Bazaar.
*/
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const vc_js_1 = __importDefault(require("vc-js"));
const { suites: { Ed25519Signature2018 } } = require('jsonld-signatures');
const _1 = require("./");
async function verifyCredential(credential, config) {
    const documentLoader = _1.getDocumentLoader(config);
    const suite = new Ed25519Signature2018({});
    const result = await vc_js_1.default.verifyCredential({
        credential,
        suite,
        documentLoader,
    });
    return result;
}
exports.verifyCredential = verifyCredential;
//# sourceMappingURL=verify.js.map