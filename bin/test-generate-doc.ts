import path from 'path';
import fs from 'fs';

const { generateDocx } = require('../lib/generate-docx');

try {
    const buf = generateDocx('test/', 'input', { 'name.first': 'Felicity', 'name.last': 'Laramir' });
    const out = path.resolve('./test/out.docx');
    fs.writeFileSync(path.resolve(__dirname, out), buf);
} catch (e) {
    throw (e);

}
