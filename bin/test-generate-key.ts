
const { writeKeys } = require('../vc/');
const issuer = process.argv[2];
const host = "http://localhost:2015";

if (!issuer) {
    throw Error(`usage: ${process.argv[1]} user`)
}

writeKeys(host, issuer);
