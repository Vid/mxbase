const db = require('../lib/db');
const [, , username, emailAddress, password] = process.argv;

console.log('creating', username, emailAddress, password);

db.createUser({ username, emailAddress, password }, (err) => {
    if (err) {
        console.error('create error', err);
        return;
    }
    db.findUserByUsername(username, (err, user) => {
        if (err) {
            console.error('find error', err);
        } else {
            console.log('created user', user);
        }
    })
});

