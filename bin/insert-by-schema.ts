// example - 
// export NODE_ENV=test  D='https://vc.solid.nicer.info' && ts-node node_modules/mxbase/bin/insert-by-schema.ts  https://vc.solid.nicer.info  ./site/database.sqlite3 site/content/pages/ visitedLink/v1 uri vid

import fs from 'fs';

import { DB } from '../lib/db';
import { schemaInNS } from '../lib/paths';
import { DataProcessor, FileCachingNetworkContextRetriever } from '../lib/data';

const [me, , host, database, dir, schema, prop, username] = process.argv;

if (!username) {
    throw Error('usage: ' + [me, 'host', 'database', 'dir', 'schema', 'prop', 'username'].join(' '));
}

doImport();

async function doImport() {
    const dp = new DataProcessor(host, new FileCachingNetworkContextRetriever(host, '.'));
    const db = new DB(database, host);

    const files = fs.readdirSync(dir)
    console.log('importing', files.length, host);
    await db.runQuery('delete from expanded');
    await db.runQuery('delete from pastExpanded');
    await db.runQuery('delete from ids');
    await db.runQuery('delete from pastIds');
    let i = 0;
    let past = 0;
    let current = 0;
    for (const f of files) {
        const json = JSON.parse(fs.readFileSync(`${dir}/${f}`, 'utf-8'));
        const existing = (await db.findExpandedRows({ predicate: schemaInNS(host, schema) + `/${prop}`, object: json[prop] }, { username }));
        if (existing && existing.length > 0) {
            past++;
            console.log('e______', i, f, existing.length)
            await db.saveExpandedRows(dp.expandToRows({ '@context': [schemaInNS(host, schema)], ...json }), { username, schema, graph: existing[0].graph });
        } else {
            current++;
            console.log('n______', i, f)
            await db.saveExpandedRows(dp.expandToRows({ '@context': [schemaInNS(host, schema)], ...json }), { username, schema });
        }
    }
    console.log({ past, current });
}