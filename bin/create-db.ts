
import { DB } from '../lib/db';

const [me, , database, host] = process.argv;

if (!host) {
    throw Error(`usage: ${me} database host`);
}

const db = new DB(database, host);

createTables();

async function createTables() {
    await db.createTables();
}
