#!/usr/bin/env node

import express from 'express';
import * as bodyParser from 'body-parser';
import * as path from 'path';
import * as fs from 'fs';
import mkdirp from 'mkdirp';

import { logger } from './lib/logging';
import { generateDocx } from './lib/generate-docx';
import { DB } from './lib/db';
import { mkpaths } from './lib/util';

import { CheckAuth } from './extensions/accounts';

export type TConfig = {
  host: string,
  site_base: string,
  schemas: { [name: string]: any }
}

export type TExtension = ({ app: Express, checkAuth: CheckAuth, getConfig: updatedConfig, db: DB, logger: logger, defaultRoute: defaultRoute }) => void;

export const mxbase = async ({ port = 2020, dbIn, extensions, startedCallback, getConfig }: { port?: number, dbIn?: any, extensions?: { [name: string]: TExtension }, startedCallback?: any, getConfig: () => TConfig }) => {
  const config = getConfig();
  let db: DB = dbIn !== undefined ? dbIn : new DB(`${config.site_base}/database.sqlite3`, config.host);
  if (!await db.isCreated()) {
    logger.info('creating site base');
    db = await initSite({ host: config.host, site_base: config.site_base, jwtText: Math.random().toString(), db: `${config.site_base}/database.sqlite3` });
  }

  // get updated config, since some getConfig may read from disk FIXME
  function updatedConfig(): Promise<TConfig> {
    return new Promise((resolve, reject) => {
      try {
        const config = getConfig();
        resolve(config);
      } catch (e) {
        logger.error(e);
        reject(`Your site is missing or has a malformed config. Please see the README`);
      }
    });
  }

  let SECRET_KEY: string;
  try {
    SECRET_KEY = fs.readFileSync(path.resolve(process.cwd(), `${config.site_base}/private/jwt.text`), 'utf-8');
  } catch (e) {
    logger.error(`please create a secret key in ${config.site_base}/private/jwt.text`);
    throw (e);
  }

  const app = express();

  app.use(bodyParser.json({ limit: '999mb' }))

  const checkAuth = new CheckAuth(SECRET_KEY, logger);
  const checker = checkAuth.checker();

  // FIXME this should go in extensions/keys
  app.use('/app/key/', express.static(`${getConfig().site_base}/keys`));

  const defaultRoute = (req, res) => {
    try {
      res.sendFile(path.resolve(process.cwd(), 'build/index.html'));
    } catch (e) {
      console.log(`can't find build`);
    }
  };

  if (extensions) {
    for (const k of Object.keys(extensions)) {
      logger.info('adding', k);
      await extensions[k]({ app, checkAuth, getConfig: updatedConfig, logger, db, defaultRoute });
    }
  }

  // log requests
  app.use((req, res, next) => {
    logger.info('access', req.originalUrl);
    next();
  });

  app.use('/app/build/', express.static(path.resolve(process.cwd(), 'build/')));
  app.use('/app/assets/', express.static(`${config.site_base}/assets`));

  app.get('/app', (req, res) => {
    res.redirect('/')
  });

  app.get('/app/config', async function (req, res) {
    const config = await updatedConfig();
    res.json(config);
  });

  app.post('/app/docx', checker, async function (req, res) {
    const { fields, template, type } = req.body;
    const buf = generateDocx(`${config.site_base}/templates/`, template, fields);
    res.set('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
    res.end(buf);
  });


  app.get('/', defaultRoute);

  startup();

  async function startup() {
    const server = await app.listen(port);
    logger.info(`app started on port ${port} with ${app._router.stack.filter(r => r.route).map(r => r.route.path).sort().join(' ')}`);
    startedCallback && startedCallback(server);
  }
}

// create a new instance of a site
export async function initSite({ host, site_base, jwtText, db }: { host: string, site_base: string, jwtText: string, db?: string }): Promise<DB> {
  mkpaths(site_base);
  // documents
  mkdirp.sync(path.join(site_base, 'a'));
  mkdirp.sync(path.join(site_base, 'cache'));
  fs.writeFileSync(`${site_base}/private/jwt.text`, jwtText);
  db = db || path.join(site_base, 'database.sqlite');
  const database = new DB(db, host);
  await database.createTables();
  return database;
}
